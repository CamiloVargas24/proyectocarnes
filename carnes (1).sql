-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-05-2018 a las 23:56:07
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `carnes`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE `ciudad` (
  `idciudad` int(10) UNSIGNED NOT NULL,
  `ciudad` varchar(50) NOT NULL,
  `estado` varchar(20) NOT NULL DEFAULT 'Activo'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ciudad`
--

INSERT INTO `ciudad` (`idciudad`, `ciudad`, `estado`) VALUES
(1, 'Tunja', 'Activo'),
(2, 'Ventaquemada', 'Activo'),
(3, 'Sogamoso', 'Activo'),
(4, 'Tierra Negra', 'Activo'),
(5, 'Somondoco', 'Activo'),
(6, 'Cali', 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `idfactura` int(10) UNSIGNED ZEROFILL NOT NULL,
  `persona_idpersona` int(10) UNSIGNED NOT NULL,
  `nit` varchar(50) DEFAULT NULL,
  `peso_res` float DEFAULT NULL COMMENT 'En libras',
  `decomiso` text,
  `causa_decomiso` text,
  `fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `total` int(10) UNSIGNED DEFAULT NULL COMMENT 'Es el total de la factura',
  `tipo_factura` varchar(100) NOT NULL DEFAULT 'Venta',
  `estado` varchar(20) NOT NULL DEFAULT 'Activo'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`idfactura`, `persona_idpersona`, `nit`, `peso_res`, `decomiso`, `causa_decomiso`, `fecha`, `total`, `tipo_factura`, `estado`) VALUES
(0000000037, 20, NULL, 0, NULL, NULL, '2018-05-24 00:00:00', 18000, 'Venta', 'Activo'),
(0000000038, 15, NULL, NULL, NULL, NULL, '2018-05-24 23:19:10', 0, 'Venta', 'Activo'),
(0000000039, 15, NULL, 0, NULL, NULL, '2018-05-24 00:00:00', 13000, 'Venta', 'Activo'),
(0000000040, 20, NULL, 0, NULL, NULL, '2018-05-24 00:00:00', 8000, 'Venta', 'Activo'),
(0000000041, 15, NULL, 0, NULL, NULL, '2018-05-24 00:00:00', 23000, 'Venta', 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura_pedido_has_producto`
--

CREATE TABLE `factura_pedido_has_producto` (
  `producto_idproducto` int(10) UNSIGNED NOT NULL,
  `factura_idfactura` int(10) UNSIGNED ZEROFILL NOT NULL,
  `peso_producto` float NOT NULL DEFAULT '1' COMMENT 'En libras',
  `subtotal` float NOT NULL COMMENT 'el costo que tiene el producto'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `factura_pedido_has_producto`
--

INSERT INTO `factura_pedido_has_producto` (`producto_idproducto`, `factura_idfactura`, `peso_producto`, `subtotal`) VALUES
(1, 0000000037, 3, 12000),
(1, 0000000039, 1, 4000),
(1, 0000000040, 2, 8000),
(1, 0000000041, 2, 8000),
(2, 0000000037, 2, 6000),
(2, 0000000039, 3, 9000),
(2, 0000000041, 5, 15000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `genero`
--

CREATE TABLE `genero` (
  `idgenero` int(10) UNSIGNED NOT NULL,
  `genero` varchar(45) NOT NULL,
  `estado` varchar(20) NOT NULL DEFAULT 'Activo'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `genero`
--

INSERT INTO `genero` (`idgenero`, `genero`, `estado`) VALUES
(1, 'Masculino', 'Activo'),
(2, 'Femenino', 'Activo'),
(3, 'Indefinido', 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `idpersona` int(10) UNSIGNED NOT NULL,
  `cedula` varchar(30) NOT NULL,
  `nombres` varchar(45) NOT NULL,
  `apellidos` varchar(45) NOT NULL,
  `direccion` varchar(75) DEFAULT NULL,
  `telefono` varchar(20) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `contrasena` varchar(100) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `tipo_usuario_idtipo_usuario` int(10) UNSIGNED NOT NULL DEFAULT '2',
  `celular` varchar(20) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `genero_idgenero` int(10) UNSIGNED NOT NULL,
  `ciudad_idciudad` int(10) UNSIGNED NOT NULL,
  `estado` varchar(20) NOT NULL DEFAULT 'Activo'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`idpersona`, `cedula`, `nombres`, `apellidos`, `direccion`, `telefono`, `contrasena`, `email`, `tipo_usuario_idtipo_usuario`, `celular`, `fecha_nacimiento`, `genero_idgenero`, `ciudad_idciudad`, `estado`) VALUES
(1, '10491234', 'Admin', 'el trebol', 'tierra negra', '3123454555', 'eltrebol', 'eltrebol@gmail.com', 2, '3123231333', '1976-02-15', 1, 2, 'Inactivo'),
(15, '213123131', 'dasdklasda', 'sdasda', 'asdasdasd', '123123123', 'sadasd', 'sadasd', 1, '21312312312', '2011-11-11', 1, 6, 'Activo'),
(16, '123123123', 'asdasda', 'adada', 'dasdasd', '1231312', '23123123123', 'eltrebol@gmail.comw', 1, '123131312313', '2009-12-12', 1, 6, 'Activo'),
(18, '1231231231', 'dadasdasda', 'dasdadada', 'dassdad', '1212131231', 'adasdadada', 'camilo.vargasc41@gmail.com', 1, '2131231312', '2009-12-19', 1, 6, 'Activo'),
(19, '1050170678', 'Miguel as', 'Pulido lol', 'carrea 12232', '31234321', '123131231', 'miguel@gmail.com2', 2, '3122312312312', '2001-11-12', 1, 2, 'Activo'),
(20, '1', 'Generico', '    ', 'Generico', '5555555555', 'generico', 'generico@gmail.com', 2, '55555555555', '1994-10-24', 1, 4, 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `idproducto` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `descripcion` text,
  `precio_actual` float NOT NULL,
  `peso_libras` float NOT NULL,
  `imagen` varchar(300) DEFAULT NULL,
  `estado` varchar(15) NOT NULL DEFAULT 'Activo'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`idproducto`, `nombre`, `descripcion`, `precio_actual`, `peso_libras`, `imagen`, `estado`) VALUES
(1, 'pierna', 'carne de pierna de res', 4000, 0, NULL, 'Activo'),
(2, 'Lomo', 'Lomo de res de cebù', 3000, 2, NULL, 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_usuario`
--

CREATE TABLE `tipo_usuario` (
  `idtipo_usuario` int(10) UNSIGNED NOT NULL,
  `tipo` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_usuario`
--

INSERT INTO `tipo_usuario` (`idtipo_usuario`, `tipo`) VALUES
(1, 'Administrador'),
(2, 'Cliente');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD PRIMARY KEY (`idciudad`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`idfactura`),
  ADD KEY `factura_pedido_FKIndex1` (`persona_idpersona`);

--
-- Indices de la tabla `factura_pedido_has_producto`
--
ALTER TABLE `factura_pedido_has_producto`
  ADD PRIMARY KEY (`producto_idproducto`,`factura_idfactura`),
  ADD KEY `factura_pedido_has_producto_FKIndex1` (`factura_idfactura`),
  ADD KEY `factura_pedido_has_producto_FKIndex2` (`producto_idproducto`);

--
-- Indices de la tabla `genero`
--
ALTER TABLE `genero`
  ADD PRIMARY KEY (`idgenero`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`idpersona`),
  ADD KEY `persona_FKIndex1` (`genero_idgenero`),
  ADD KEY `persona_FKIndex2` (`ciudad_idciudad`),
  ADD KEY `persona_FKIndex3` (`tipo_usuario_idtipo_usuario`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`idproducto`);

--
-- Indices de la tabla `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  ADD PRIMARY KEY (`idtipo_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  MODIFY `idciudad` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `factura`
--
ALTER TABLE `factura`
  MODIFY `idfactura` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT de la tabla `genero`
--
ALTER TABLE `genero`
  MODIFY `idgenero` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `idpersona` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `idproducto` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  MODIFY `idtipo_usuario` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `factura_ibfk_1` FOREIGN KEY (`persona_idpersona`) REFERENCES `persona` (`idpersona`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `factura_pedido_has_producto`
--
ALTER TABLE `factura_pedido_has_producto`
  ADD CONSTRAINT `factura_pedido_has_producto_ibfk_1` FOREIGN KEY (`factura_idfactura`) REFERENCES `factura` (`idfactura`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `factura_pedido_has_producto_ibfk_2` FOREIGN KEY (`producto_idproducto`) REFERENCES `producto` (`idproducto`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `persona`
--
ALTER TABLE `persona`
  ADD CONSTRAINT `persona_ibfk_1` FOREIGN KEY (`genero_idgenero`) REFERENCES `genero` (`idgenero`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `persona_ibfk_2` FOREIGN KEY (`ciudad_idciudad`) REFERENCES `ciudad` (`idciudad`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `persona_ibfk_3` FOREIGN KEY (`tipo_usuario_idtipo_usuario`) REFERENCES `tipo_usuario` (`idtipo_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
