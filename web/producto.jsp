<%-- 
    Document   : producto
    Created on : 30-may-2018, 19:22:27
    Author     : Itachi
--%>
<% if(session.getAttribute("usuario") != null){ %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Producto</title>
        <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="js/producto.js"></script>
    </head>
    <body>
        <div class = "container">
            <div class="row">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="index.jsp">
                                <img alt="Brand" src="img/logo.png" style="width: 2%">
                            </a>
                        </div>
                    </div>
                </nav>
                Bienvenido ${sessionScope.usuario}
                <a href="cerrarsesion">Salir</a>
                <div id="title-producto" class="col-md-12">
                    <h1>Producto</h1>
                </div>
                <div class="col-md-6 col-md-offset-3 content-producto">
                    <form name="formProducto" id="formProducto">
                        <input type="hidden" name="id" id="id">
                        <label for="">ID Producto:</label>
                        <input class="form-control" type="number" name="idproducto" id="idproducto">
                        <label for="">Nombre:</label>
                        <input class="form-control" type="text" name="nombre" id="nombre">
                        <label for="">Descripción</label>
                        <textarea class="form-control" name="descripcion" id="descripcion"></textarea>
                        <label for="">Precio Actual</label>
                        <input class="form-control" type="number" name="precio" id="precio">
                        <label for="">Peso en Libras</label>
                        <input class="form-control" type="number" name="peso" id="peso">
                        <label for="">Estado:</label>
                        <input class="form-control" name="estado" id="estado" value="Activo" readonly>
                        
                        
                        
                        
                    </form>
                </div>
                <div id="btn-admin" class="col-md-8 col-md-offset-2">
                    <div class="col-md-3">
                       <input class="btn" type="submit" name="consultar" value="Consultar Producto" id="btnConsultar"> 
                    </div>
                    <div class="col-md-3">
                        <input class="btn" type="submit" name="crear" value="Crear Producto" id="btnCrear">
                    </div>
                    <div class="col-md-3"> 
                        <input class="btn" type="submit" name="modificar" value="Modificar Producto" id="btnModificar">
                    </div>
                    <div class="col-md-3">
                        <input class="btn" type="submit" name="inactivar" value="Inactivar Producto" id="btnInactivar">
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="footer">
            <p>Cerniceria el trebol. 2018</p>
        </div>   
    </body>
    <style>
        #title-producto{
            text-align: center;
        }
        #btn-admin{
            margin-top: 12px;
        }
        #btnConsultar{
            color: #fff;
            background-color: #0b7dda;
        }
        #btnCrear{
            color: #fff;
            background-color: #4CAF50;
        }
        #btnModificar{
            color: #fff;
            background-color: #ff9800;
        }
        #btnInactivar{
            color: #fff;
            background-color: #f44336;
        }
        .footer {
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;
            background-color: #219610;
            color: white;
            text-align: center;
        }
    </style>
</html>
<%}else{
    String redirectURL = "http://localhost:8080/proyectoCarnes/login.jsp";
    response.sendRedirect(redirectURL);
} %>