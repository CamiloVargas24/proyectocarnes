<%-- 
    Document   : persona
    Created on : 19-may-2018, 10:43:10
    Author     : Itachi
--%>
<% if(session.getAttribute("usuario") != null){ %>
<%@page import="controlador.ControladorCiudad"%>
<%@page import="controlador.ControladorGenero"%>
<%@page import="controlador.ControladorTipoUsuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Persona</title>
        <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="js/persona.js"></script>
        <script src = " https://unpkg.com/sweetalert/dist/sweetalert.min.js "> </script>
    </head>
    <body>
        <div class = "container">
            <div  class = "row">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="index.jsp">
                                <img alt="Brand" src="img/logo.png" style="width: 2%">
                            </a>
                        </div>
                    </div>
                </nav>
                Bienvenido ${sessionScope.usuario}
                <a href="cerrarsesion">Salir</a>
                <div id="title-persona" class="col-md-12">
                    <h3>Persona</h3>
                </div>
                <div class="col-md-12 content-persona">

                    <form name="formPersona" id="formPersona">
                        <input type="hidden" name="id" id="id">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">ID Persona:</label>
                                    <input class="form-control" type="number" name="idpersona" id="idpersona">
                                </div>
                                <div class="form-group">
                                    <label for="">Cédula:</label>
                                    <input class="form-control" type="number" name="cedula" id="cedula">
                                </div>
                                <div class="form-group">
                                    <label for="">Nombres:</label>
                                    <input class="form-control" type="text" name="nombres" id="nombres" value="${persona}">
                                </div>
                                <div class="form-group">
                                    <label for="">Apellidos:</label>
                                    <input class="form-control" type="text" name="apellidos" id="apellidos">
                                </div>
                                <div class="form-group">
                                    <label for="">Direccion:</label>
                                    <input class="form-control" type="text" name="direccion" id="direccion">
                                </div>
                                <div class="form-group">
                                    <label  for="">Teléfono:</label>
                                    <input class="form-control" type="number" name="telefono" id="telefono">
                                </div>
                                <div class="form-group">
                                    <label for="">Email:</label>
                                    <input class="form-control" type="email" name="email" id="email">
                                </div>

                            </div>
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label for="">Contraseña:</label>
                                    <input class="form-control" type="password" name="contrasena" id="contrasena">
                                </div>
                                <div class="form-group">
                                    <label for="">Tipo Usuario:</label>
                                    <select class="form-control" name="tipoUsuario" id="tipoUsuario">
                                        <% ControladorTipoUsuario ctu = new ControladorTipoUsuario();%>
                                        <%= ctu.getViewTipoUsuarios()%>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Celular:</label>
                                    <input class="form-control" type="number" name="celular" id="celular">
                                </div>
                                <div class="form-group">
                                    <label for="">Fecha de Nacimiento:</label>
                                    <input class="form-control" type="date" name="fechaNacimiento" id="fechaNacimiento">
                                </div>
                                <div class="form-group">
                                    <label for="">Género:</label>
                                    <select class="form-control" name="genero" id="genero">
                                        <% ControladorGenero cg = new ControladorGenero();%>
                                        <%= cg.getViewGeneros()%>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Ciudad:</label>
                                    <select class="form-control" name="ciudad" id="ciudad">
                                        <% ControladorCiudad cc = new ControladorCiudad();%>
                                        <%= cc.getViewCiudades()%>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Estado:</label>
                                    <input class="form-control" name="estado" id="estado" value="Activo" readonly>
                                </div>


                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <input type="submit" name="crear" value="Crear Persona" id="btnCrear">
                            </div>
                            <div class="col-md-3">
                                <input type="submit" name="consultar" value="Consultar Persona" id="btnConsultar">
                            </div>
                            <div class="col-md-3">
                                <input type="submit" name="modificar" value="Modificar Persona" id="btnModificar">
                            </div>
                            <div class="col-md-3">
                                <input type="submit" name="inactivar" value="Inactivar Persona" id="btnInactivar">
                            </div>
                        </div>
                    </form>
                </div>                              
            </div>

        </div>
        <br><br>
        <div class="footer">
            <p>Cerniceria el trebol. 2018</p>
        </div>
    </body>
    <style>
        .content-persona{
            padding: 0px 200px;
        }
        tr {
            margin-bottom: 10px;
        }
        #title-persona{
            text-align: center; 
            background-color: #f0ffed;
        }
        #btnCrear{
            background-color: #4CAF50;
            color: #fff;
            width: 100%;
            padding: 5px;
        }
        #btnConsultar{
            background-color: #0b7dda;
            color: #fff;
            width: 100%;
            padding: 5px;

        }
        #btnModificar{
            background-color: #ff9800;
            color: #fff;
            width: 100%;
            padding: 5px;
        }
        #btnInactivar{
            background-color: #f44336;
            color: #fff;
            width: 100%;
            padding: 5px;
        }
        .footer {
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;
            background-color: #219610;
            color: white;
            text-align: center;
        }
    </style>
</html>
<%}else{
    String redirectURL = "http://localhost:8080/proyectoCarnes/login.jsp";
    response.sendRedirect(redirectURL);
} %>