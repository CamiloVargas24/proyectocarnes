<%-- 
    Document   : index
    Created on : 31-may-2018, 18:34:18
    Author     : camilo
--%>
<% if (session.getAttribute("usuario") != null) { %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <title>Carniceria el Trebol</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="#">
                                <img alt="Brand" src="img/logo.png" style="width: 2%">
                            </a>
                        </div>
                    </div>
                </nav>
                Bienvenido ${sessionScope.usuario}
                <a href="cerrarsesion">Salir</a>
                <div id="contenedor-inicio">
                    <div class="jumbotron" style="background-color: #f0ffed;">
                        <h1>Bienvenido a Carniceria el Trebol</h1>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <div class="servicios">
                                <img id="image-person" src="img/icono-persona.png" class="img-fluid" alt="Responsive image">
                                <button id="btn-persona" type="button" class="btn btn-primary btn-lg btn-block" onclick="location.href = 'persona.jsp'">Persona</button>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="servicios">
                                <img id="image-producto" src="img/icono-venta.png" class="img-fluid" alt="Responsive image"><br>
                                <button id="btn-producto" type="button" class="btn btn-primary btn-lg btn-block" onclick="location.href = 'producto.jsp'">Producto</button>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="servicios">
                                <img id="image-venta" src="img/iconoventa.svg" class="img-fluid" alt="Responsive image"><br>
                                <button id="btn-venta" type="button" class="btn btn-primary btn-lg btn-block" onclick="location.href = 'venta.jsp'">Venta</button>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="servicios">
                                <img id="image-pedido" src="img/icono-pedido.png" class="img-fluid" alt="Responsive image"><br>
                                <button id="btn-pedido" type="button" class="btn btn-primary btn-lg btn-block" onclick="location.href = 'pedido.jsp'">Pedido</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
            <p>Cerniceria el trebol. 2018</p>
        </div>
    </body>

    <style>
        .jumbotron{
            text-align: center;
            width: 100%;
        }
        #contenedor-inicio{
            text-align: center;
        }
        .servicios{
            width: 90%;
            height: 200px;
            border: 1px solid;
            border-radius: 6px;
        }
        #image-person{
            width: 220px;
        }
        #image-producto{
            width: 113px;
            margin-top: 33px;
        }
        #image-venta{
            width: 113px;
            margin-top: 33px;
        }
        #btn-persona{
            margin-top: 15px;
        }
        #btn-producto{
            margin-top: 6px;
        }
        #btn-venta{
            margin-top: 6px;
        }
        #image-pedido{
            width: 94px;
            margin-top: 50px;
        }
        #btn-pedido{
            margin-top: 8px;
        }
        .footer {
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;
            background-color: #219610;
            color: white;
            text-align: center;
        }
    </style>

</html>
<%} else {
        String redirectURL = "http://localhost:8080/proyectoCarnes/login.jsp";
        response.sendRedirect(redirectURL);
    }%>
