<%-- 
    Document   : pedido
    Created on : 23-may-2018, 19:02:28
    Author     : Itachi
--%>
<% if (session.getAttribute("usuario") != null) { %>
<%@page import="controlador.ControladorProducto"%>
<%@page import="controlador.ControladorPersona"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Pedido</title>
        <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="js/pedido.js"></script>
    </head>
    <body>
        <div class = "container">
            <form name="formPedido" id="formVenta">
                <input type="hidden" name="id" id="id">
                <div class = "row">
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <a class="navbar-brand" href="index.jsp">
                                    <img alt="Brand" src="img/logo.png" style="width: 2%">
                                </a>
                            </div>
                        </div>
                    </nav>
                    Bienvenido ${sessionScope.usuario}
                    <a href="cerrarsesion">Salir</a>
                    <div id="title-venta" class="col-md-12">
                        <h1>Pedido</h1>
                    </div>
                    <div class="col-md-6 col-md-offset-3">

                        <label for="">ID Pedido</label>
                        <input class="form-control" type="number" name="idfactura" id="idfactura">
                        <label for="">Proveedor</label>
                        <select class="form-control" name="idpersona" id="idpersona">
                            <% ControladorPersona cp = new ControladorPersona();%>
                            <%= cp.getViewProveedores()%>
                        </select>
                        <label for="">Peso total de la res en Libras</label>
                        <input class="form-control" type="number" name="peso_total" id="peso_total">
                        <input type="hidden" name="decomisado" id="decomisado" value="N">
                        <div class="checkbox">
                            <label><input type="checkbox" name="decomisado_check" id="decomisado_check">¿Fué decomisado?</label>
                        </div>
                        <textarea class="form-control" rows="5" name="causa_decomiso" id="causa_decomiso" disabled></textarea>
                        <input type="submit" name="inactivar" value="Inactivar Pedido" id="btnInactivar">
                    </div>
                    <div class="col-md-6 col-md-offset-3 consultar-venta">
                        <input class="btn" type="submit" name="consultar" value="Consultar Pedido" id="btnConsultar">
                        <input class="btn" type="submit" name="nuevo" value="Nuevo Pedido" id="btnNuevo">
                    </div>
                    <div class="col-md-12">
                        <table class="table" id="tablaDetalleProductos" border="1" cellspacing="1" cellpadding="1">
                            <tbody>
                                <tr id="encabezado">
                                    <td>Producto</td>
                                    <td>Cantidad (Lbs)</td>
                                    <td>Precio Unitario</td>
                                    <td>Subtotal</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <select class="form-control" name="producto" id="producto">
                                            <% ControladorProducto cprod = new ControladorProducto();%>
                                            <%= cprod.getViewProductos()%>
                                        </select>
                                    </td>
                                    <td>
                                        <input class="form-control" name="cantidad" id="cantidad" type="number" placeholder="En libras">
                                    </td>
                                    <td>
                                        <input class="form-control" name="precioUnitario" id="precioUnitario" type="number" placeholder="Precio unitario">
                                    </td>
                                    <td id="subtotal">-</td>
                                    <td><input class="form-control" type="submit" name="adicionar" value="+" id="btnAdicionar"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </form>
        </div><br><br>
        <div class="footer">
            <p>Cerniceria el trebol. 2018</p>
        </div>
    </body>
    <style>
        #title-venta{
            text-align: center;
        }
        .consultar-venta{
            text-align: center;
            margin-top: 12px;
            margin-bottom: 12px;
        }
        #encabezado{
            background-color: #dee2e6;
            font-weight: bold;
        }#btnAdicionar{
            background-color: green;
            color: #fff;
        }
        .footer {
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;
            margin-top: 10px;
            background-color: #219610;
            color: white;
            text-align: center;
        }
    </style>
</html>
<%} else {
        String redirectURL = "http://localhost:8080/proyectoCarnes/login.jsp";
        response.sendRedirect(redirectURL);
    }%>