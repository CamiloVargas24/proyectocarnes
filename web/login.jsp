<%-- 
    Document   : login
    Created on : 30-may-2018, 23:16:10
    Author     : Itachi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
        <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="js/login.js"></script>
    </head>
    <body>
        <div class="wrapper">
            <form class="form-signin" id="formLogin">       
                <h2 class="form-signin-heading">CARNICERIA EL TREBOL</h2>
                <input type="text" class="form-control" id="email" name="email" placeholder="Email" required="" autofocus="" /><br>
                <input type="password" class="form-control" id="contrasena" name="contrasena" placeholder="Contraseña" required=""/>   <br>   
                <input class="btn btn-lg btn-primary btn-block" name="entrar" value="Entrar" id="btnEntrar">
            </form>
             
        </div>
   
    </body>
</html>
<style>
    body {
        background: #eee !important;	
    }

    .wrapper {	
        margin-top: 80px;
        margin-bottom: 80px;
    }

    .form-signin {
        max-width: 431px;
        padding: 15px 35px 45px;
        margin: 0 auto;
        background-color: #fff;
        border: 1px solid rgba(0,0,0,0.1);  

        .form-signin-heading,
        .checkbox {
            margin-bottom: 30px;
        }

        .checkbox {
            font-weight: normal;
        }

        .form-control {
            position: relative;
            font-size: 16px;
            height: auto;
            padding: 10px;
        }

        input[type="text"] {
            margin-bottom: 5px !important;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }

        input[type="password"] {
            margin-bottom: 20px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
    }

</style>
