/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function() {
    $('#btnModificar').hide();
    $('#btnInactivar').hide();
    function validarCampos() {
        var errores = 0;
        var cedula = $('#cedula').val();
        var nombres = $('#nombres').val();
        var apellidos = $('#apellidos').val();
        var direccion = $('#direccion').val();
        var telefono = $('#telefono').val();
        var contrasena = $('#contrasena').val();
        var email = $('#email').val();
        var tipoUsuario = $('#tipoUsuario').val();
        var celular = $('#celular').val();
        var fechaNacimiento = $('#fechaNacimiento').val();
        var genero = $('#genero').val();
        var ciudad = $('#ciudad').val();
        var estado = $('#estado').val();

        if (cedula.length < 5) {
            alert("Digite la cédula, mínimo 5 dígitos");
            $("#cedula").focus();
            errores += 1;
        } else if (nombres.length < 3) {
            alert("Digite los nombres, mínimo 3 dígitos");
            $("#nombres").focus();
            errores += 1;
        } else if (apellidos.length < 3) {
            alert("Digite los apellidos, mínimo 3 dígitos");
            $("#apellidos").focus();
            errores += 1;
        } else if (direccion.length < 5) {
            alert("Digite la dirección, mínimo 5 dígitos");
            $("#direccion").focus();
            errores += 1;
        } else if (telefono.length < 7) {
            alert("Digite el teléfono, mínimo 7 dígitos");
            $("#telefono").focus();
            errores += 1;
        } else if (email.length < 6) {
            alert("Digite el email, mínimo 6 dígitos");
            $("#email").focus();
            errores += 1;
        } else if (contrasena.length < 5) {
            alert("Digite la contraseña, mínimo 5 dígitos");
            $("#contrasena").focus();
            errores += 1;
        } else if (tipoUsuario.length == 0) {
            alert("Seleccione un usuario");
            errores += 1;
        } else if (celular.length < 10) {
            alert("Digite el número de celular, mínimo 10 dígitos");
            $("#celular").focus();
            errores += 1;
        } else if (fechaNacimiento.length < 5) {
            alert("Digite una fecha de nacimiento válida");
            $("#fechaNacimiento").focus();
            errores += 1;
        } else if (genero.length == 0) {
            alert("Seleccione un genero");
            errores += 1;
        } else if (ciudad.length == 0) {
            alert("Seleccione una ciudad");
            errores += 1;
        } else if (estado.length == 0) {
            alert("Seleccione un estado");
            errores += 1;
        }
        if (errores >= 1) {
            return false;
        } else {
            return true;
        }
    }

    $('#btnCrear').click(function(e) {
        e.preventDefault();

        if (validarCampos()) {
            var data = $("form#formPersona").serialize() + '&accion=crear';

            $.ajax({
                url: "persona",
                type: "POST",
                data: data,
                success: function(data) {
                    alert(data);
                    location.reload();
                },
                error: function(e) {
                    alert(e.statusText);
                },
            })
        }



    });
    $('#btnConsultar').click(function(e) {
        e.preventDefault();
        var idpersona = $("#idpersona").val();
        var cedula = $("#cedula").val();
        if (idpersona === "" && cedula === "") {
            alert("Digite el id o cédula");
        } else {
            var data = 'idpersona=' + idpersona + '&cedula=' + cedula + '&accion=consultar';
            $.ajax({
                url: "persona",
                type: "POST",
                data: data,
                success: function(data) {
                    var rta = jQuery.parseJSON(data);

                    $('#id').val(rta.idpersona);
                    $('#idpersona').val(rta.idpersona);
                    $('#cedula').val(rta.cedula);
                    $('#nombres').val(rta.nombres);
                    $('#apellidos').val(rta.apellidos);
                    $('#direccion').val(rta.direccion);
                    $('#telefono').val(rta.telefono);
                    $('#contrasena').val(rta.contrasena);
                    $('#email').val(rta.email);
                    $('#tipoUsuario').val(rta.tipo_usuario_idtipo_usuario.idtipo_usuario);
                    $('#celular').val(rta.celular);
                    var mydate = new Date(rta.fecha_nacimiento);
                    var fecha = mydate.toISOString();
                    var dates = fecha.substring(0, 10).split('-')
                    var dateConFormato = dates[0] + '-' + dates[1] + '-' + dates[2]

                    $('#fechaNacimiento').val(dateConFormato);
                    $('#genero').val(rta.genero_idgenero["idgenero"]);
                    $('#ciudad').val(rta.ciudad_idciudad.idciudad);
                    $('#estado').val(rta.estado);
                    $('#btnModificar').show();
                    $('#btnInactivar').show();
                    if (rta.estado === 'Activo') {
                        $('#btnInactivar').val("Inactivar Persona");
                    } else {
                        $('#btnInactivar').val("Activar Persona");
                    }
                },
                error: function(e) {
                    alert(e.statusText);
                },
            })
        }
    });
    
    $('#btnModificar').click(function(e) {
        e.preventDefault();
        var idpersona = $("#idpersona").val();
        var cedula = $("#cedula").val();
        if (idpersona === "" && cedula === "") {
            alert("Digite el id o cédula");
        } else {
            var data = $("form#formPersona").serialize() + '&accion=modificar';
            $.ajax({
                url: "persona",
                type: "POST",
                data: data,
                success: function(data) {
                    alert(data);
                    location.reload();
                },
                error: function(e) {
                    alert(e.statusText);
                },
            })
        }
    });
    $('#btnInactivar').click(function(e) {
        e.preventDefault();
        var idpersona = $("#idpersona").val();
        if (idpersona === "") {
            alert("Digite el id");
        } else {
            var data = $("form#formPersona").serialize() + '&accion=inactivar';
            $.ajax({
                url: "persona",
                type: "POST",
                data: data,
                success: function(data) {
                    alert(data);
                    location.reload();
                },
                error: function(e) {
                    alert(e.statusText);
                },
            })
        }
    });
})
