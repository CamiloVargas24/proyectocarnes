/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function () {
    $('#btnInactivar').hide();
    $('#btnNuevo').hide();
    if ($('#id').val() != null) {
        $('#btnNuevo').show();
    }
    $('#producto').on('change', function (e) {

        if (this.value !== 'Seleccione') {
            e.preventDefault();

            var data = 'idproducto=' + this.value + '&accion=consultarProducto';

            $.ajax({
                url: "venta",
                type: "POST",
                data: data,
                success: function (data) {
                    var rta = jQuery.parseJSON(data);
                    $('#precioUnitario').html(rta.precio_actual);
                    $('#subtotal').html(rta.precio_actual);
                    $('#cantidad').val('1');
                },
                error: function (e) {
                    alert(e.statusText);
                },
            });
        } else {
            $('#precioUnitario').html('-');
        }
    });

    function calcularPrecios(precio) {
        var preciovalor = $(precio).val();
        var cantidad = $('#precioUnitario').html();
        var subtotal = preciovalor * cantidad;
        $('#subtotal').html(subtotal);
    }

    $("#cantidad").on('keyup', function (e) {
        calcularPrecios(this);
    });
    $("#cantidad").change(function (e) {
        calcularPrecios(this);
    });

    $('#btnAdicionar').click(function (e) {
        e.preventDefault();
        var id = $("#id").val();
        var idpersona = $('#idpersona').val();
        var producto = $('#producto').val();
        var cantidad = $('#cantidad').val();
        var idfactura = 0;
        if (cantidad > 0) {
            if (id === "") { //SI DAN CLIC POR PRIMER VEZ CREA LA FACTURA
                var data = 'idpersona=' + idpersona + '&accion=guardarFactura';
                $.ajax({
                    url: "venta",
                    type: "POST",
                    data: data,
                    async: false,
                    success: function (data) {
                        console.log(data)
                        var rta = jQuery.parseJSON(data);
                        console.log(rta)
                        $('#id').val(rta);
                        idfactura = rta;
                    },
                    error: function (e) {
                        alert(e.statusText);
                    },
                });
            }
            if (idfactura === 0) {
                idfactura = $('#id').val();
            }
            var data = 'idfactura=' + idfactura + '&cantidad=' + cantidad + '&producto=' + producto + '&accion=agregarDetalleFactura';
            //DETALLE FACTURA
            $.ajax({
                url: "detallefactura",
                type: "POST",
                data: data,
                async: false,
                success: function (data) {
                    $('#tablaDetalleProductos tr').slice(2).remove();
                    jQuery("#tablaDetalleProductos").append(data);
                },
                error: function (e) {
                    alert(e.statusText);
                },
            });
        } else {
            alert('Digite una cantidad válida');
        }
    });
    $('#btnConsultar').click(function (e) {
        e.preventDefault();
        var idfactura = $('#idfactura').val();
        if (idfactura > 0) {
            var data = 'idfactura=' + idfactura + '&accion=consultarVenta';
            $.ajax({
                url: "venta",
                type: "POST",
                data: data,
                async: false,
                success: function (data) {
                    var rta = jQuery.parseJSON(data);
                    $('#id').val(rta.idfactura);
                    $('#idfactura').val(rta.idfactura);
                    $('#idpersona').val(rta.persona_idpersona.idpersona);
                    
                    var data = 'idfactura=' + idfactura + '&accion=consultarDetalleFactura';
                    
                    //DETALLE FACTURA
                    $.ajax({
                        url: "detallefactura",
                        type: "POST",
                        data: data,
                        async: false,
                        success: function (data) {
                            $('#tablaDetalleProductos tr').slice(2).remove();
                            jQuery("#tablaDetalleProductos").append(data);
                            $("#id").val(idfactura);
                        },
                        error: function (e) {
                            alert(e.statusText);
                        },
                    });
                },
                error: function (e) {
                    alert(e.statusText);
                },
            });


        } else {
            alert('Ingrese el codigo de la venta');
        }
    });
});
function eliminarProductoFactura(e) {
    e.preventDefault();
    var idfactura = e.currentTarget.attributes.idfactura.value;
    var idproducto = e.currentTarget.attributes.idproducto.value;
    console.log(idfactura);
    console.log(idproducto);
    if (idfactura != null && idproducto != null) {
        var data = 'idfactura=' + idfactura + '&idproducto=' + idproducto + '&accion=eliminarProductoDetalleFactura';
        //Eliminar producto de la factura
        $.ajax({
            url: "detallefactura",
            type: "POST",
            data: data,
            async: false,
            success: function (data) {
                console.log(data)
                $('#tablaDetalleProductos tr').slice(2).remove();
                jQuery("#tablaDetalleProductos").append(data);
            },
            error: function (e) {
                alert(e.statusText);
            },
        });
    } else {
        alert("Error, no se encuentra el idfactura o idproducto")
    }
}
;


