/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function () {
    $('#btnInactivar').hide();
    $('#btnNuevo').hide();
    if ($('#id').val() != null) {
        $('#btnNuevo').show();
    }
    $('#decomisado_check').on('change', function (e) {
        if ($(this).is(':checked')) {
            $("#causa_decomiso").removeAttr("disabled");
            $("#decomisado").val("S");
        } else {
            $("#causa_decomiso").attr("disabled", "");
            $("#causa_decomiso").val("");
            $("#decomisado").val("N");
        }
    });
    $('#producto').on('change', function (e) {

        if (this.value !== 'Seleccione') {
            e.preventDefault();

            var data = 'idproducto=' + this.value + '&accion=consultarProducto';

            $.ajax({
                url: "venta",
                type: "POST",
                data: data,
                success: function (data) {
                    var rta = jQuery.parseJSON(data);
                    $('#precioUnitario').val(rta.precio_actual);
                    $('#subtotal').html(rta.precio_actual);
                    $('#cantidad').val('1');
                },
                error: function (e) {
                    alert(e.statusText);
                },
            });
        } else {
            $('#precioUnitario').html('-');
        }
    });

    function calcularPrecios(precio) {
        var preciovalor = $(precio).val();
        var cantidad = $('#precioUnitario').val();
        var subtotal = preciovalor * cantidad;
        $('#subtotal').html(subtotal);
    }

    function calcularPreciosNuevos(precio) {
        var preciovalor = $(precio).val();
        var cantidad = $('#cantidad').val();
        var subtotal = preciovalor * cantidad;
        $('#subtotal').html(subtotal);
    }

    $("#cantidad").on('keyup', function (e) {
        calcularPrecios(this);
    });
    $("#cantidad").change(function (e) {
        calcularPrecios(this);
    });

    $("#precioUnitario").on('keyup', function (e) {
        calcularPreciosNuevos(this);
    });
    $("#precioUnitario").change(function (e) {
        calcularPreciosNuevos(this);
    });
    $('#btnNuevo').click(function (e) {
        e.preventDefault();
        window.location.reload(true);
    });

    $('#btnAdicionar').click(function (e) {
        e.preventDefault();
        var id = $("#id").val();
        var idpersona = $('#idpersona').val();
        var peso_total = $('#peso_total').val();
        var producto = $('#producto').val();
        var cantidad = $('#cantidad').val();
        var decomisado = $('#decomisado').val();
        var causa_decomiso = $('#causa_decomiso').val();
        var idfactura = 0;
        if (cantidad > 0) {
            if (id === "") { //SI DAN CLIC POR PRIMER VEZ CREA LA FACTURA
                var data = 'idpersona=' + idpersona + '&peso_total=' + peso_total + '&decomisado=' + decomisado + '&causa_decomiso=' + causa_decomiso + '&accion=guardarFactura';
                $.ajax({
                    url: "pedido",
                    type: "POST",
                    data: data,
                    async: false,
                    success: function (data) {
                        var rta = jQuery.parseJSON(data);
                        $('#id').val(rta);
                        $('#idfactura').val(rta);
                        $('#idpersona').attr('disabled', '');
                        $('#peso_total').attr('disabled', '');
                        $('#decomisado_check').attr('disabled', '');
                        $('#causa_decomiso').attr('disabled', '');
                        $('#btnNuevo').show();
                        idfactura = rta;
                    },
                    error: function (e) {
                        alert(e.statusText);
                    },
                });
            }
            if (idfactura === 0) {
                idfactura = $('#id').val();
            }
            var precio = $('#precioUnitario').val();
            var data = 'idfactura=' + idfactura + '&cantidad=' + cantidad + '&producto=' + producto + '&precioNuevo=' + precio +'&accion=agregarDetalleFacturaPedido';
            //DETALLE FACTURA
            $.ajax({
                url: "detallefactura",
                type: "POST",
                data: data,
                async: false,
                success: function (data) {
                    $('#tablaDetalleProductos tr').slice(2).remove();
                    jQuery("#tablaDetalleProductos").append(data);
                },
                error: function (e) {
                    alert(e.statusText);
                },
            });
        } else {
            alert('Digite una cantidad válida');
        }
    });
    
    $('#btnConsultar').click(function (e) {
        e.preventDefault();
        var idfactura = $('#idfactura').val();
        if (idfactura > 0) {
            //BUSCAR FACTURA
            var data = 'idfactura=' + idfactura + '&accion=consultarPedido';
            $.ajax({
                url: "pedido",
                type: "POST",
                data: data,
                async: false,
                success: function (data) {
                    var rta = jQuery.parseJSON(data);
                    $('#id').val(rta.idfactura);
                    $('#idfactura').val(rta.idfactura);
                    $('#idpersona').val(rta.persona_idpersona.idpersona);
                    $('#peso_total').val(rta.peso_res);
                    $("#causa_decomiso").attr("disabled", "");
                    $('#decomisado_check').val(rta.decomiso);
                    if (rta.decomiso === 'S') {
                        $('#decomisado_check').attr('checked', 'checked');
                        $("#decomisado").val("S");
                    } else {
                        $("#decomisado").val("N");
                        $('#decomisado_check').removeAttr('checked');
                    }

                    $('#decomisado').val(rta.decomiso);
                    $('#causa_decomiso').val(rta.causa_decomiso);
                    $('#btnNuevo').show();

                    //CONSULTAR DETALLE FACTURA
                    var data = 'idfactura=' + idfactura + '&accion=consultarDetalleFactura';
                    $.ajax({
                        url: "detallefactura",
                        type: "POST",
                        data: data,
                        async: false,
                        success: function (data) {
                            $('#tablaDetalleProductos tr').slice(2).remove();
                            jQuery("#tablaDetalleProductos").append(data);
                            $("#id").val(idfactura);
                        },
                        error: function (e) {
                            alert(e.statusText);
                        },
                    });
                },
                error: function (e) {
                    alert(e.statusText);
                },
            });

        } else {
            alert('Ingrese el codigo del pedido');
        }
    });
});
function eliminarProductoFactura(e) {
    e.preventDefault();
    var idfactura = e.currentTarget.attributes.idfactura.value;
    var idproducto = e.currentTarget.attributes.idproducto.value;
    if (idfactura != null && idproducto != null) {
        var data = 'idfactura=' + idfactura + '&idproducto=' + idproducto + '&accion=eliminarProductoDetalleFacturaPedido';
        //Eliminar producto de la factura
        $.ajax({
            url: "detallefactura",
            type: "POST",
            data: data,
            async: false,
            success: function (data) {
                console.log(data)
                $('#tablaDetalleProductos tr').slice(2).remove();
                jQuery("#tablaDetalleProductos").append(data);
            },
            error: function (e) {
                alert(e.statusText);
            },
        });
    } else {
        alert("Error, no se encuentra el idfactura o idproducto")
    }
}
;


