/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function() {
//    $('#btnModificar').hide();
//    $('#btnInactivar').hide();
    function validarCampos() {
        var errores = 0;
        var nombre = $('#nombre').val();
        var precio = $('#precio').val();
        var peso = $('#peso').val();
        var estado = $('#estado').val();

        if (nombre.length < 3) {
            alert("Digite el nombre del producto, mínimo 3 dígitos");
            $("#nombre").focus();
            errores += 1;
        } else if (precio.length < 2) {
            alert("Digite el precio del producto, mínimo 2 dígitos");
            $("#precio").focus();
            errores += 1;
        } else if (peso.length < 1) {
            alert("Digite el peso del producto, mínimo 1 dígito");
            $("#peso").focus();
            errores += 1;
        } else if (estado.length == 0) {
            alert("Seleccione un estado");
            errores += 1;
        }
        if (errores >= 1) {
            return false;
        } else {
            return true;
        }
    }

    $('#btnCrear').click(function(e) {
        e.preventDefault();

        if (validarCampos()) {
            var data = $("form#formProducto").serialize() + '&accion=crear';

            $.ajax({
                url: "producto",
                type: "POST",
                data: data,
                success: function(data) {
                    alert(data);
                    location.reload();
                },
                error: function(e) {
                    alert(e.statusText);
                },
            })
        }
    });
    
    $('#btnConsultar').click(function(e) {
        e.preventDefault();
        var idproducto = $("#idproducto").val();
        if (idproducto === "") {
            alert("Digite el id del producto");
        } else {
            var data = 'idproducto=' + idproducto + '&accion=consultar';
            $.ajax({
                url: "producto",
                type: "POST",
                data: data,
                success: function(data) {
                    var rta = jQuery.parseJSON(data);

                    $('#id').val(rta.idproducto);
                    $('#idproducto').val(rta.idproducto);
                    $('#nombre').val(rta.nombre);
                    $('#descripcion').val(rta.descripcion);
                    $('#precio').val(rta.precio_actual);
                    $('#peso').val(rta.peso_libras);
                    $('#estado').val(rta.estado);
                    $('#btnModificar').show();
                    $('#btnInactivar').show();
                    if (rta.estado === 'Activo') {
                        $('#btnInactivar').val("Inactivar Producto");
                    } else {
                        $('#btnInactivar').val("Activar Producto");
                    }
                },
                error: function(e) {
                    alert(e.statusText);
                },
            })
        }
    });
    $('#btnModificar').click(function(e) {
        e.preventDefault();
        var idproducto = $("#idproducto").val();
        if (idproducto === "") {
            alert("Digite el id del producto");
        } else {
            var data = $("form#formProducto").serialize() + '&accion=modificar';
            $.ajax({
                url: "producto",
                type: "POST",
                data: data,
                success: function(data) {
                    alert(data);
                    location.reload();
                },
                error: function(e) {
                    alert(e.statusText);
                },
            })
        }
    });
    $('#btnInactivar').click(function(e) {
        e.preventDefault();
        var idproducto = $("#id").val();
        if (idproducto === "") {
            alert("Digite el id del producto");
        } else {
            var data = $("form#formProducto").serialize() + '&accion=inactivar';
            $.ajax({
                url: "producto",
                type: "POST",
                data: data,
                success: function(data) {
                    alert(data);
                    location.reload();
                },
                error: function(e) {
                    alert(e.statusText);
                },
            })
        }
    });
})
