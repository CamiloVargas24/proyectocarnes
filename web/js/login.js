/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function() {
    function validarCampos() {
        var errores = 0;
        var email = $('#email').val();
        var contrasena = $('#contrasena').val();
        
        if (email.length < 5) {
            alert("Digite un correo válido, mínimo 5 dígitos");
            $("#email").focus();
            errores += 1;
        } else if (contrasena.length < 3) {
            alert("Digite una contraseña válida, mínimo 3 dígitos");
            $("#contrasena").focus();
            errores += 1;
        }
        if (errores >= 1) {
            return false;
        } else {
            return true;
        }
    }

    $('#btnEntrar').click(function(e) {
        e.preventDefault();
        if (validarCampos()) {
            var data = $("form#formLogin").serialize() + '&accion=entrar';
            $.ajax({
                url: "login",
                type: "POST",
                data: data,
                success: function(data) {
                    window.location.href = "http://localhost:8080/proyectoCarnes/";
                },
                error: function(e) {
                    alert(e.statusText);
                },
            })
        }
    });
})
