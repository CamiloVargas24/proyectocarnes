/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import include.Factura;
import include.Persona;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author Itachi
 */
public class ModeloFactura extends Conexion {

    public boolean crearFacturaVenta(Factura f) {
        PreparedStatement pst = null;
        boolean creado = false;
        try {
            String consulta = "INSERT INTO factura(persona_idpersona,"
                    + " total, tipo_factura, estado) "
                    + "VALUES(?,?,?,?)";
            pst = getConnection().prepareStatement(consulta);
            pst.setInt(1, f.getPersona_idpersona().getIdpersona());
            pst.setInt(2, 0);
            pst.setString(3, f.getTipo_factura());
            pst.setString(4, f.getEstado());

            if (pst.executeUpdate() == 1) {
                creado = true;
            }
        } catch (Exception e) {
            System.err.println("error crearFacturaVenta:" + e);
        }
        return creado;
    }

    public boolean crearFacturaPedido(Factura f) {
        PreparedStatement pst = null;
        boolean creado = false;
        try {
            String consulta = "INSERT INTO factura(persona_idpersona, peso_res, decomiso,causa_decomiso,"
                    + " total, tipo_factura, estado) "
                    + "VALUES(?,?,?,?,?,?,?)";
            pst = getConnection().prepareStatement(consulta);
            pst.setInt(1, f.getPersona_idpersona().getIdpersona());
            pst.setFloat(2, f.getPeso_res());
            pst.setString(3, f.getDecomiso());
            pst.setString(4, f.getCausa_decomiso());
            pst.setInt(5, 0);
            pst.setString(6, f.getTipo_factura());
            pst.setString(7, f.getEstado());

            if (pst.executeUpdate() == 1) {
                creado = true;
            }
        } catch (Exception e) {
            System.err.println("error crearFacturaPedido:" + e);
        }
        return creado;
    }

    public boolean buscarFactura(Integer idfactura) {
        PreparedStatement pst = null;
        ResultSet rs = null;
        boolean encontrado = false;
        try {
            String sql = "SELECT * FROM factura WHERE idfactura = ?";
            pst = getConnection().prepareStatement(sql);
            pst.setInt(1, idfactura);
            rs = pst.executeQuery();
            while (rs.next()) {
                encontrado = true;
            }
        } catch (Exception e) {
            System.err.println("error buscarFactura:" + e);
        } finally {
            try {
                if (getConnection() != null) {
                    getConnection().close();
                }
                if (pst != null) {
                    pst.close();
                }
            } catch (Exception e) {
                System.err.println("error buscarFactura " + e);
            }
        }
        return encontrado;
    }

    public Factura consultarFactura(Integer idFactura) {
        PreparedStatement pst = null;
        ResultSet rs = null;
        Factura factura = null;
        try {
            String sql = "SELECT * FROM factura WHERE idfactura = ? LIMIT 1";
            pst = getConnection().prepareStatement(sql);
            pst.setInt(1, idFactura);
            rs = pst.executeQuery();

            while (rs.next()) {
                Persona persona = new Persona();
                persona.setIdpersona(rs.getInt("persona_idpersona"));

                factura = new Factura(idFactura, persona, rs.getString("nit"), rs.getFloat("peso_res"), rs.getString("decomiso"), rs.getString("causa_decomiso"), rs.getDate("fecha"), rs.getFloat("total"), rs.getString("tipo_factura"), rs.getString("estado"));
            }
        } catch (Exception e) {
            System.err.println("error consultarFactura:" + e);
        } finally {
            try {
                if (getConnection() != null) {
                    getConnection().close();
                }
                if (pst != null) {
                    pst.close();
                }
            } catch (Exception e) {
                System.err.println("error consultarFactura" + e);
            }
        }
        return factura;
    }

    public Integer consultarUltimoIDFactura() {
        PreparedStatement pst = null;
        ResultSet rs = null;
        Integer idFactura = 0;
        try {
            String sql = "SELECT idfactura FROM factura ORDER BY idfactura DESC LIMIT 1";
            pst = getConnection().prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                idFactura = rs.getInt("idfactura");
            }
        } catch (Exception e) {
            System.err.println("error consultarUltimoIDFactura: " + e);
        } finally {
            try {
                if (getConnection() != null) {
                    getConnection().close();
                }
                if (pst != null) {
                    pst.close();
                }
            } catch (Exception e) {
                System.err.println("error consultarUltimoIDFactura " + e);
            }
        }
        return idFactura;
    }

    public boolean modificarFactura(Factura f) {
        PreparedStatement pst = null;
        boolean modificado = false;
        try {
            String consulta = "UPDATE factura SET nit=?,"
                    + "peso_res=?,decomiso=?,causa_decomiso=?,fecha=?,total=?,"
                    + "tipo_factura=?,estado=? WHERE idfactura = ?";
            pst = getConnection().prepareStatement(consulta);
            pst.setString(1, f.getNit());
            pst.setFloat(2, f.getPeso_res());
            pst.setString(3, f.getDecomiso());
            pst.setString(4, f.getCausa_decomiso());
            pst.setDate(5, f.getFecha());
            pst.setFloat(6, f.getTotal());
            pst.setString(7, f.getTipo_factura());
            pst.setString(8, f.getEstado());
            pst.setInt(9, f.getIdfactura());

            if (pst.executeUpdate() == 1) {
                modificado = true;
            }
        } catch (Exception e) {
            System.err.println("error modificarFactura: " + e);
        } finally {
            try {
                if (getConnection() != null) {
                    getConnection().close();
                }
                if (pst != null) {
                    pst.close();
                }
            } catch (Exception e) {
                System.err.println("error modificarFactura " + e);
            }
        }
        return modificado;
    }
}
