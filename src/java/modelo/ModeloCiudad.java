/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import include.Ciudad;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Itachi
 */
public class ModeloCiudad extends Conexion {

    public ArrayList<Ciudad> getAllCiudad() {
        ArrayList<Ciudad> ciudades = new ArrayList<>();
        PreparedStatement pst = null;
        ResultSet rs = null;
        try {
            String sql = "SELECT * FROM ciudad WHERE estado LIKE 'Activo' ORDER BY ciudad ASC";
            pst = getConnection().prepareStatement(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                ciudades.add(new Ciudad(rs.getInt("idciudad"), rs.getString("ciudad"), rs.getString("estado")));
            }
        } catch (Exception e) {
            System.err.println("error getAllCiudad:" + e);
        } finally {
            try {
                if (getConnection() != null) {
                    getConnection().close();
                }
                if (pst != null) {
                    pst.close();
                }
            } catch (Exception e) {
                System.err.println("error " + e);
            }
        }
        return ciudades;
    }

}
