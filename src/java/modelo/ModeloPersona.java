/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import include.Ciudad;
import include.Genero;
import include.Persona;
import include.TipoUsuario;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Itachi
 */
public class ModeloPersona extends Conexion {

    public boolean crearPersona(Persona p) {
        PreparedStatement pst = null;
        boolean creado = false;
        try {
            String consulta = "INSERT INTO persona (cedula, nombres, apellidos, direccion, telefono, contrasena,"
                    + "email, tipo_usuario_idtipo_usuario, celular, fecha_nacimiento, genero_idgenero, 	ciudad_idciudad, estado) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
            pst = getConnection().prepareStatement(consulta);
            pst.setString(1, p.getCedula());
            pst.setString(2, p.getNombres());
            pst.setString(3, p.getApellidos());
            pst.setString(4, p.getDireccion());
            pst.setString(5, p.getTelefono());
            pst.setString(6, p.getContrasena());
            pst.setString(7, p.getEmail());
            pst.setObject(8, p.getTipo_usuario_idtipo_usuario().getIdtipo_usuario());
            pst.setString(9, p.getCelular());
            pst.setDate(10, p.getFecha_nacimiento());
            pst.setObject(11, p.getGenero_idgenero().getIdgenero());
            pst.setObject(12, p.getCiudad_idciudad().getIdciudad());
            pst.setString(13, p.getEstado());

            if (pst.executeUpdate() == 1) {
                creado = true;
            }
        } catch (Exception e) {
            System.err.println("error crearPersona:" + e);
        } finally {
            try {
                if (getConnection() != null) {
                    getConnection().close();
                }
                if (pst != null) {
                    pst.close();
                }
            } catch (Exception e) {
                System.err.println("error crearPersona" + e);
            }
        }
        return creado;
    }

    public boolean buscarPersona(String parametro) {
        PreparedStatement pst = null;
        ResultSet rs = null;
        boolean encontrado = false;
        try {
            String sql = "SELECT * FROM persona WHERE idpersona = ? OR cedula = ?";
            pst = getConnection().prepareStatement(sql);
            pst.setInt(1, Integer.parseInt(parametro));
            pst.setString(2, parametro);
            rs = pst.executeQuery();
            while (rs.next()) {
                encontrado = true;
            }
        } catch (Exception e) {
            System.err.println("error buscarPersona:" + e);
        } finally {
            try {
                if (getConnection() != null) {
                    getConnection().close();
                }
                if (pst != null) {
                    pst.close();
                }
            } catch (Exception e) {
                System.err.println("error buscarPersona " + e);
            }
        }
        return encontrado;
    }

    public Persona consultarPersona(String parametro) {
        PreparedStatement pst = null;
        ResultSet rs = null;
        Persona persona = null;
        try {
            String sql = "SELECT * FROM persona WHERE idpersona = ? OR cedula = ? OR email = ? LIMIT 1";
            pst = getConnection().prepareStatement(sql);
            pst.setInt(1, Integer.parseInt(parametro));
            pst.setString(2, parametro);
            pst.setString(3, parametro);
            rs = pst.executeQuery();

            while (rs.next()) {
                TipoUsuario tipousuario = new TipoUsuario();
                tipousuario.setIdtipo_usuario(rs.getInt("tipo_usuario_idtipo_usuario"));
                Genero genero = new Genero();
                genero.setIdgenero(rs.getInt("genero_idgenero"));
                Ciudad ciudad = new Ciudad();
                ciudad.setIdciudad(rs.getInt("ciudad_idciudad"));
                persona = new Persona(rs.getInt("idpersona"), rs.getString("cedula"), rs.getString("nombres"), rs.getString("apellidos"), rs.getString("direccion"), rs.getString("telefono"), rs.getString("contrasena"), rs.getString("email"), tipousuario, rs.getString("celular"), rs.getDate("fecha_nacimiento"), genero, ciudad, rs.getString("estado"));
            }
        } catch (Exception e) {
            System.err.println("error consultarPersona:" + e);
        } finally {
            try {
                if (getConnection() != null) {
                    getConnection().close();
                }
                if (pst != null) {
                    pst.close();
                }
            } catch (Exception e) {
                System.err.println("error consultarPersona " + e);
            }
        }
        return persona;
    }

    public boolean modificarPersona(Persona p) {
        PreparedStatement pst = null;
        boolean creado = false;
        try {
            String consulta = "UPDATE persona SET cedula=?, nombres=?,apellidos=?,"
                    + "direccion=?,telefono=?,contrasena=?,email=?,tipo_usuario_idtipo_usuario=?,celular=?,"
                    + "fecha_nacimiento=?,genero_idgenero=?,ciudad_idciudad=? WHERE idpersona = ?";
            pst = getConnection().prepareStatement(consulta);
            pst.setString(1, p.getCedula());
            pst.setString(2, p.getNombres());
            pst.setString(3, p.getApellidos());
            pst.setString(4, p.getDireccion());
            pst.setString(5, p.getTelefono());
            pst.setString(6, p.getContrasena());
            pst.setString(7, p.getEmail());
            pst.setObject(8, p.getTipo_usuario_idtipo_usuario().getIdtipo_usuario());
            pst.setString(9, p.getCelular());
            pst.setDate(10, p.getFecha_nacimiento());
            pst.setObject(11, p.getGenero_idgenero().getIdgenero());
            pst.setObject(12, p.getCiudad_idciudad().getIdciudad());
            pst.setInt(13, p.getIdpersona());

            if (pst.executeUpdate() == 1) {
                creado = true;
            }
        } catch (Exception e) {
            System.err.println("error modificarPersona:" + e);
        } finally {
            try {
                if (getConnection() != null) {
                    getConnection().close();
                }
                if (pst != null) {
                    pst.close();
                }
            } catch (Exception e) {
                System.err.println("error modificarPersona " + e);
            }
        }
        return creado;
    }

    public boolean inactivarPersona(Persona p) {
        PreparedStatement pst = null;
        boolean modificado = false;
        try {
            String consulta = "UPDATE persona SET estado=? WHERE idpersona = ?";
            pst = getConnection().prepareStatement(consulta);
            pst.setString(1, p.getEstado());
            pst.setInt(2, p.getIdpersona());

            if (pst.executeUpdate() == 1) {
                modificado = true;
            }
        } catch (Exception e) {
            System.err.println("error inactivarPersona:" + e);
        } finally {
            try {
                if (getConnection() != null) {
                    getConnection().close();
                }
                if (pst != null) {
                    pst.close();
                }
            } catch (Exception e) {
                System.err.println("error inactivarPersona " + e);
            }
        }
        return modificado;
    }

    public ArrayList<Persona> getAllPersona(String concatenarSql) {
        ArrayList<Persona> personas = new ArrayList<>();
        PreparedStatement pst = null;
        ResultSet rs = null;
        try {
            String sql = "SELECT * FROM persona WHERE estado LIKE 'Activo'"+concatenarSql;
            pst = getConnection().prepareStatement(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                TipoUsuario tipousuario = new TipoUsuario();
                tipousuario.setIdtipo_usuario(rs.getInt("tipo_usuario_idtipo_usuario"));
                Genero genero = new Genero();
                genero.setIdgenero(rs.getInt("genero_idgenero"));
                Ciudad ciudad = new Ciudad();
                ciudad.setIdciudad(rs.getInt("ciudad_idciudad"));
                personas.add(new Persona(rs.getInt("idpersona"), rs.getString("cedula"), rs.getString("nombres"), rs.getString("apellidos"), rs.getString("direccion"), rs.getString("telefono"), rs.getString("contrasena"), rs.getString("email"), tipousuario, rs.getString("celular"), rs.getDate("fecha_nacimiento"), genero, ciudad, rs.getString("estado")));
            }
        } catch (Exception e) {
            System.err.println("error getAllPersona:" + e);
        } finally {
            try {
                if (getConnection() != null) {
                    getConnection().close();
                }
                if (pst != null) {
                    pst.close();
                }
            } catch (Exception e) {
                System.err.println("error getAllPersona " + e);
            }
        }
        return personas;
    }
    
    public Persona loginPersona(String email) {
        PreparedStatement pst = null;
        ResultSet rs = null;
        Persona persona = null;
        try {
            String sql = "SELECT * FROM persona WHERE email = ? LIMIT 1";
            pst = getConnection().prepareStatement(sql);
            pst.setString(1, email);
            rs = pst.executeQuery();

            while (rs.next()) {
                TipoUsuario tipousuario = new TipoUsuario();
                tipousuario.setIdtipo_usuario(rs.getInt("tipo_usuario_idtipo_usuario"));
                Genero genero = new Genero();
                genero.setIdgenero(rs.getInt("genero_idgenero"));
                Ciudad ciudad = new Ciudad();
                ciudad.setIdciudad(rs.getInt("ciudad_idciudad"));
                persona = new Persona(rs.getInt("idpersona"), rs.getString("cedula"), rs.getString("nombres"), rs.getString("apellidos"), rs.getString("direccion"), rs.getString("telefono"), rs.getString("contrasena"), rs.getString("email"), tipousuario, rs.getString("celular"), rs.getDate("fecha_nacimiento"), genero, ciudad, rs.getString("estado"));
            }
        } catch (Exception e) {
            System.err.println("error loginPersona:" + e);
        } finally {
            try {
                if (getConnection() != null) {
                    getConnection().close();
                }
                if (pst != null) {
                    pst.close();
                }
            } catch (Exception e) {
                System.err.println("error loginPersona " + e);
            }
        }
        return persona;
    }
}
