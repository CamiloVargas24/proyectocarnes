/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import include.DetalleFactura;
import include.Factura;
import include.Producto;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Itachi
 */
public class ModeloDetalleFactura extends Conexion {

    public boolean crearDetalleFactura(DetalleFactura df) {
        PreparedStatement pst = null;
        boolean creado = false;
        try {
            String consulta = "INSERT INTO factura_pedido_has_producto "
                    + "(producto_idproducto, factura_idfactura, peso_producto, subtotal) "
                    + "VALUES(?,?,?,?)";
            pst = getConnection().prepareStatement(consulta);
            pst.setObject(1, df.getProducto_idproducto().getIdproducto());
            pst.setObject(2, df.getFactura_idfactura().getIdfactura());
            pst.setFloat(3, df.getPeso_producto());
            pst.setFloat(4, df.getSubtotal());

            if (pst.executeUpdate() == 1) {
                creado = true;
            }
        } catch (Exception e) {
            System.err.println("error crearDetalleFactura:" + e);
        } finally {
            try {
                if (getConnection() != null) {
                    getConnection().close();
                }
                if (pst != null) {
                    pst.close();
                }
            } catch (Exception e) {
                System.err.println("error crearDetalleFactura" + e);
            }
        }
        return creado;
    }

    public DetalleFactura consultarDetalleFactura(Integer idfactura, Integer idproducto) {
        PreparedStatement pst = null;
        ResultSet rs = null;
        DetalleFactura detalleFactura = null;
        try {
            String sql = "SELECT * FROM factura_pedido_has_producto WHERE factura_idfactura = ? AND producto_idproducto = ? LIMIT 1";
            pst = getConnection().prepareStatement(sql);
            pst.setInt(1, idfactura);
            pst.setInt(2, idproducto);
            rs = pst.executeQuery();

            while (rs.next()) {

                Factura factura = new Factura();
                factura.setIdfactura(rs.getInt("factura_idfactura"));

                Producto producto = new Producto();
                producto.setIdproducto(rs.getInt("producto_idproducto"));

                detalleFactura = new DetalleFactura(producto, factura, rs.getFloat("peso_producto"), rs.getFloat("subtotal"));
            }
        } catch (Exception e) {
            System.err.println("error consultarDetalleFactura:" + e);
        } finally {
            try {
                if (getConnection() != null) {
                    getConnection().close();
                }
                if (pst != null) {
                    pst.close();
                }
            } catch (Exception e) {
                System.err.println("error consultarDetalleFactura " + e);
            }
        }
        return detalleFactura;
    }
    
    public ArrayList<DetalleFactura> consultarDetalleFactura(Integer idFactura) {
        ArrayList<DetalleFactura> detalleFactura = new ArrayList<>();
        PreparedStatement pst = null;
        ResultSet rs = null;
        try {
            String sql = "SELECT * FROM factura_pedido_has_producto WHERE factura_idfactura = ?";
            pst = getConnection().prepareStatement(sql);
            pst.setInt(1, idFactura);
            rs = pst.executeQuery();
            while (rs.next()) {
                Producto producto = new Producto();
                producto.setIdproducto(rs.getInt("producto_idproducto"));
                Factura factura = new Factura();
                factura.setIdfactura(rs.getInt("factura_idfactura"));
                detalleFactura.add(new DetalleFactura(producto, factura, rs.getFloat("peso_producto"), rs.getFloat("subtotal")));
            }
        } catch (Exception e) {
            System.err.println("error consultarDetalleFactura:" + e);
        } finally {
            try {
                if (getConnection() != null) {
                    getConnection().close();
                }
                if (pst != null) {
                    pst.close();
                }
            } catch (Exception e) {
                System.err.println("error consultarDetalleFactura" + e);
            }
        }
        return detalleFactura;
    }

    public boolean eliminarDetalleFactura(Integer idfactura, Integer idproducto) {
        PreparedStatement pst = null;
        boolean eliminado = false;
        try {
            String consulta = "DELETE FROM factura_pedido_has_producto "
                    + "WHERE factura_idfactura = ? AND producto_idproducto = ?";
            pst = getConnection().prepareStatement(consulta);
            pst.setInt(1, idfactura);
            pst.setInt(2, idproducto);

            if (pst.executeUpdate() == 1) {
                eliminado = true;
            }
        } catch (Exception e) {
            System.err.println("error eliminarDetalleFactura:" + e);
        } finally {
            try {
                if (getConnection() != null) {
                    getConnection().close();
                }
                if (pst != null) {
                    pst.close();
                }
            } catch (Exception e) {
                System.err.println("error eliminarDetalleFactura" + e);
            }
        }
        return eliminado;
    }
}
