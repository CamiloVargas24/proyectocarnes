/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import include.Genero;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Itachi
 */
public class ModeloGenero extends Conexion {

    public ArrayList<Genero> getAllGenero() {
        ArrayList<Genero> generos = new ArrayList<>();
        PreparedStatement pst = null;
        ResultSet rs = null;
        try {
            String sql = "SELECT * FROM genero WHERE estado LIKE 'Activo'";
            pst = getConnection().prepareStatement(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                generos.add(new Genero(rs.getInt("idgenero"), rs.getString("genero"), rs.getString("estado")));
            }
        } catch (Exception e) {
            System.err.println("error getAllGenero:" + e);
        } finally {
            try {
                if (getConnection() != null) {
                    getConnection().close();
                }
                if (pst != null) {
                    pst.close();
                }
            } catch (Exception e) {
                System.err.println("error getAllGenero" + e);
            }
        }
        return generos;
    }
}
