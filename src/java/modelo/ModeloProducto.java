/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import include.Producto;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Itachi
 */
public class ModeloProducto extends Conexion {

    public boolean crearProducto(Producto p) {
        PreparedStatement pst = null;
        boolean creado = false;
        try {
            String consulta = "INSERT INTO producto (idproducto, nombre, descripcion, precio_actual, peso_libras, imagen,"
                    + "estado) VALUES(?,?,?,?,?,?,?)";
            pst = getConnection().prepareStatement(consulta);
            if(p.getIdproducto() != null){
                pst.setInt(1, p.getIdproducto());
            }else{
                 pst.setString(1, null);
            }
            
            pst.setString(2, p.getNombre());
            pst.setString(3, p.getDescripcion());
            pst.setFloat(4, p.getPrecio_actual());
            pst.setFloat(5, p.getPeso_libras());
            pst.setString(6, p.getImagen());
            pst.setString(7, p.getEstado());
           
            if (pst.executeUpdate() == 1) {
                creado = true;
            }
        } catch (Exception e) {
            System.err.println("error crearProducto:" + e);
        } finally {
            try {
                if (getConnection() != null) {
                    getConnection().close();
                }
                if (pst != null) {
                    pst.close();
                }
            } catch (Exception e) {
                System.err.println("error crearProducto" + e);
            }
        }
        return creado;
    }
    
    public boolean buscarProducto(Integer idproducto) {
        PreparedStatement pst = null;
        ResultSet rs = null;
        boolean encontrado = false;
        try {
            String sql = "SELECT * FROM producto WHERE idproducto = ?";
            pst = getConnection().prepareStatement(sql);
            pst.setInt(1, idproducto);
            rs = pst.executeQuery();
            while (rs.next()) {
                encontrado = true;
            }
        } catch (Exception e) {
            System.err.println("error buscarProducto:" + e);
        } finally {
            try {
                if (getConnection() != null) {
                    getConnection().close();
                }
                if (pst != null) {
                    pst.close();
                }
            } catch (Exception e) {
                System.err.println("error buscarProducto " + e);
            }
        }
        return encontrado;
    }

    public Producto consultarProducto(Integer idproducto) {
        PreparedStatement pst = null;
        ResultSet rs = null;
        Producto producto = null;
        try {
            String sql = "SELECT * FROM producto WHERE idproducto = ? LIMIT 1";
            pst = getConnection().prepareStatement(sql);
            pst.setInt(1, idproducto);
            rs = pst.executeQuery();
            while (rs.next()) {
                producto = new Producto(rs.getInt("idproducto"), rs.getString("nombre"), rs.getString("descripcion"), rs.getFloat("precio_actual"), rs.getFloat("peso_libras"), rs.getString("imagen"), rs.getString("estado"));
            }
        } catch (Exception e) {
            System.err.println("error consultarProducto:" + e);
        } finally {
            try {
                if (getConnection() != null) {
                    getConnection().close();
                }
                if (pst != null) {
                    pst.close();
                }
            } catch (Exception e) {
                System.err.println("error consultarProducto " + e);
            }
        }
        return producto;
    }

    public boolean modificarProducto(Producto p) {
        PreparedStatement pst = null;
        boolean modificado = false;
        try {
            String consulta = "UPDATE producto SET idproducto=?, nombre=?, descripcion=?,"
                    + "precio_actual=?,peso_libras=?,imagen=?,estado=? WHERE idproducto = ?";
            pst = getConnection().prepareStatement(consulta);
            pst.setInt(1, p.getIdproducto());
            pst.setString(2, p.getNombre());
            pst.setString(3, p.getDescripcion());
            pst.setFloat(4, p.getPrecio_actual());
            pst.setFloat(5, p.getPeso_libras());
            pst.setString(6, p.getImagen());
            pst.setString(7, p.getEstado());
            pst.setObject(8, p.getIdproducto());

            if (pst.executeUpdate() == 1) {
                modificado = true;
            }
        } catch (Exception e) {
            System.err.println("error modificarProducto " + e);
        } finally {
            try {
                if (getConnection() != null) {
                    getConnection().close();
                }
                if (pst != null) {
                    pst.close();
                }
            } catch (Exception e) {
                System.err.println("error modificarProducto " + e);
            }
        }
        return modificado;
    }
    
    public boolean inactivarProducto(Producto p) {
        PreparedStatement pst = null;
        boolean modificado = false;
        try {
            String consulta = "UPDATE producto SET estado=? WHERE idproducto = ?";
            pst = getConnection().prepareStatement(consulta);
            pst.setString(1, p.getEstado());
            pst.setInt(2, p.getIdproducto());

            if (pst.executeUpdate() == 1) {
                modificado = true;
            }
        } catch (Exception e) {
            System.err.println("error inactivarProducto:" + e);
        } finally {
            try {
                if (getConnection() != null) {
                    getConnection().close();
                }
                if (pst != null) {
                    pst.close();
                }
            } catch (Exception e) {
                System.err.println("error inactivarProducto " + e);
            }
        }
        return modificado;
    }

    public ArrayList<Producto> getAllProducto() {
        ArrayList<Producto> productos = new ArrayList<>();
        PreparedStatement pst = null;
        ResultSet rs = null;
        try {
            String sql = "SELECT * FROM producto WHERE estado LIKE 'Activo'";
            pst = getConnection().prepareStatement(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                productos.add(new Producto(rs.getInt("idproducto"), rs.getString("nombre"), rs.getString("descripcion"), rs.getFloat("precio_actual")));
            }
        } catch (Exception e) {
            System.err.println("error getAllProducto:" + e);
        } finally {
            try {
                if (getConnection() != null) {
                    getConnection().close();
                }
                if (pst != null) {
                    pst.close();
                }
            } catch (Exception e) {
                System.err.println("error getAllProducto " + e);
            }
        }
        return productos;
    }
}
