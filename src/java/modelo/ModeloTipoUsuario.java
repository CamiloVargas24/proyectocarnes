/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import include.TipoUsuario;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Itachi
 */
public class ModeloTipoUsuario extends Conexion {

    public ArrayList<TipoUsuario> getAllTipoUsuario() {
        ArrayList<TipoUsuario> tipousuarios = new ArrayList<>();
        PreparedStatement pst = null;
        ResultSet rs = null;
        try {
            String sql = "SELECT * FROM tipo_usuario";
            pst = getConnection().prepareStatement(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                tipousuarios.add(new TipoUsuario(rs.getInt("idtipo_usuario"), rs.getString("tipo")));
            }
        } catch (Exception e) {
            System.err.println("error getAllTipoUsuario:" + e);
        } finally {
            try {
                if (getConnection() != null) {
                    getConnection().close();
                }
                if (pst != null) {
                    pst.close();
                }
            } catch (Exception e) {
                System.err.println("error getAllTipoUsuario" + e);
            }
        }
        return tipousuarios;
    }

}
