/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package include;

/**
 *
 * @author Itachi
 */
public class Producto {
    private Integer idproducto;
    private String nombre;
    private String descripcion;
    private float precio_actual;
    private float peso_libras;
    private String imagen;
    private String estado;

    public Producto() {
    }

    public Producto(Integer idproducto, String nombre, String descripcion, float precio_actual) {
        this.idproducto = idproducto;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.precio_actual = precio_actual;
    }

    public Producto(Integer idproducto, String nombre, String descripcion, float precio_actual, float peso_libras, String imagen, String estado) {
        this.idproducto = idproducto;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.precio_actual = precio_actual;
        this.peso_libras = peso_libras;
        this.imagen = imagen;
        this.estado = estado;
    }

    public Integer getIdproducto() {
        return idproducto;
    }

    public void setIdproducto(Integer idproducto) {
        this.idproducto = idproducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getPrecio_actual() {
        return precio_actual;
    }

    public void setPrecio_actual(float precio_actual) {
        this.precio_actual = precio_actual;
    }

    public float getPeso_libras() {
        return peso_libras;
    }

    public void setPeso_libras(float peso_libras) {
        this.peso_libras = peso_libras;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
}
