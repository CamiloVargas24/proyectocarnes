/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package include;

import java.io.Serializable;
import java.sql.Date;
import java.text.SimpleDateFormat;

/**
 *
 * @author Itachi
 */
public class Persona {

    private Integer idpersona;
    private String cedula;
    private String nombres;
    private String apellidos;
    private String direccion;
    private String telefono;
    private String contrasena;
    private String email;
    private TipoUsuario tipo_usuario_idtipo_usuario;
    private String celular;
    private Date fecha_nacimiento;
    private Genero genero_idgenero;
    private Ciudad ciudad_idciudad;
    private String estado;

    public Persona() {
    }

    public Persona(Integer idpersona) {
        this.idpersona = idpersona;
    }

    public Persona(String cedula, String nombres, String apellidos, String direccion, String telefono, String contrasena, String email, TipoUsuario tipo_usuario_idtipo_usuario, String celular, Date fecha_nacimiento, Genero genero_idgenero, Ciudad ciudad_idciudad, String estado) {
        this.cedula = cedula;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.direccion = direccion;
        this.telefono = telefono;
        this.contrasena = contrasena;
        this.email = email;
        this.tipo_usuario_idtipo_usuario = tipo_usuario_idtipo_usuario;
        this.celular = celular;
        this.fecha_nacimiento = fecha_nacimiento;
        this.genero_idgenero = genero_idgenero;
        this.ciudad_idciudad = ciudad_idciudad;
        this.estado = estado;
    }

    public Persona(Integer idpersona, String cedula, String nombres, String apellidos, String direccion, String telefono, String contrasena, String email, TipoUsuario tipo_usuario_idtipo_usuario, String celular, Date fecha_nacimiento, Genero genero_idgenero, Ciudad ciudad_idciudad) {
        this.idpersona = idpersona;
        this.cedula = cedula;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.direccion = direccion;
        this.telefono = telefono;
        this.contrasena = contrasena;
        this.email = email;
        this.tipo_usuario_idtipo_usuario = tipo_usuario_idtipo_usuario;
        this.celular = celular;
        this.fecha_nacimiento = fecha_nacimiento;
        this.genero_idgenero = genero_idgenero;
        this.ciudad_idciudad = ciudad_idciudad;
        this.estado = estado;
    }

    public Persona(Integer idpersona, String cedula, String nombres, String apellidos, String direccion, String telefono, String contrasena, String email, TipoUsuario tipo_usuario_idtipo_usuario, String celular, Date fecha_nacimiento, Genero genero_idgenero, Ciudad ciudad_idciudad, String estado) {
        this.idpersona = idpersona;
        this.cedula = cedula;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.direccion = direccion;
        this.telefono = telefono;
        this.contrasena = contrasena;
        this.email = email;
        this.tipo_usuario_idtipo_usuario = tipo_usuario_idtipo_usuario;
        this.celular = celular;
        this.fecha_nacimiento = fecha_nacimiento;
        this.genero_idgenero = genero_idgenero;
        this.ciudad_idciudad = ciudad_idciudad;
        this.estado = estado;
    }

    public Integer getIdpersona() {
        return idpersona;
    }

    public void setIdpersona(Integer idpersona) {
        this.idpersona = idpersona;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public TipoUsuario getTipo_usuario_idtipo_usuario() {
        return tipo_usuario_idtipo_usuario;
    }

    public void setTipo_usuario_idtipo_usuario(TipoUsuario tipo_usuario_idtipo_usuario) {
        this.tipo_usuario_idtipo_usuario = tipo_usuario_idtipo_usuario;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public Date getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public void setFecha_nacimiento(Date fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public Genero getGenero_idgenero() {
        return genero_idgenero;
    }

    public void setGenero_idgenero(Genero genero_idgenero) {
        this.genero_idgenero = genero_idgenero;
    }

    public Ciudad getCiudad_idciudad() {
        return ciudad_idciudad;
    }

    public void setCiudad_idciudad(Ciudad ciudad_idciudad) {
        this.ciudad_idciudad = ciudad_idciudad;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

}
