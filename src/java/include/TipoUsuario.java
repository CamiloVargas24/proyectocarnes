/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package include;

import java.io.Serializable;

/**
 *
 * @author Itachi
 */
public class TipoUsuario implements Serializable{
    private Integer idtipo_usuario;
    private String tipo;

    public TipoUsuario() {
    }

    public TipoUsuario(Integer idtipo_usuario) {
        this.idtipo_usuario = idtipo_usuario;
    }

    public TipoUsuario(Integer idtipo_usuario, String tipo) {
        this.idtipo_usuario = idtipo_usuario;
        this.tipo = tipo;
    }

    public Integer getIdtipo_usuario() {
        return idtipo_usuario;
    }

    public void setIdtipo_usuario(Integer idtipo_usuario) {
        this.idtipo_usuario = idtipo_usuario;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
}
