/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package include;

import java.io.Serializable;

/**
 *
 * @author Itachi
 */
public class Ciudad implements Serializable {
    private Integer idciudad;
    private String ciudad;
    private String estado;

    public Ciudad() {
    }
    
    public Ciudad(Integer idciudad, String ciudad, String estado) {
        this.idciudad = idciudad;
        this.ciudad = ciudad;
        this.estado = estado;
    }

    public Ciudad(Integer idciudad) {
        this.idciudad = idciudad;
    }

    public Integer getIdciudad() {
        return idciudad;
    }

    public void setIdciudad(Integer idciudad) {
        this.idciudad = idciudad;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    
    
    
}
