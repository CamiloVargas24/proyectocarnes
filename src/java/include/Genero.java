/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package include;

import java.io.Serializable;

/**
 *
 * @author Itachi
 */
public class Genero implements Serializable {
    private Integer idgenero;
    private String genero;
    private String estado;

    public Genero() {
    }

    public Genero(Integer idgenero) {
        this.idgenero = idgenero;
    }

    public Genero(Integer idgenero, String genero, String estado) {
        this.idgenero = idgenero;
        this.genero = genero;
        this.estado = estado;
    }

    public Integer getIdgenero() {
        return idgenero;
    }

    public void setIdgenero(Integer idgenero) {
        this.idgenero = idgenero;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
}
