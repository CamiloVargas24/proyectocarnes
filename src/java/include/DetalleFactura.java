/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package include;

/**
 *
 * @author Itachi
 */
public class DetalleFactura {
    private Producto producto_idproducto;
    private Factura factura_idfactura;
    private float peso_producto;
    private float subtotal;

    public DetalleFactura() {
    }

    public DetalleFactura(Producto producto_idproducto, Factura factura_idfactura, float preso_producto, float subtotal) {
        this.producto_idproducto = producto_idproducto;
        this.factura_idfactura = factura_idfactura;
        this.peso_producto = preso_producto;
        this.subtotal = subtotal;
    }

    public Producto getProducto_idproducto() {
        return producto_idproducto;
    }

    public void setProducto_idproducto(Producto producto_idproducto) {
        this.producto_idproducto = producto_idproducto;
    }

    public Factura getFactura_idfactura() {
        return factura_idfactura;
    }

    public void setFactura_idfactura(Factura factura_idfactura) {
        this.factura_idfactura = factura_idfactura;
    }

    public float getPeso_producto() {
        return peso_producto;
    }

    public void setPeso_producto(float peso_producto) {
        this.peso_producto = peso_producto;
    }

    public float getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(float subtotal) {
        this.subtotal = subtotal;
    }
    
}
