/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package include;

import java.sql.Date;

/**
 *
 * @author Itachi
 */
public class Factura {
    private Integer idfactura;
    private Persona persona_idpersona;
    private String nit;
    private float peso_res;
    private String decomiso;
    private String causa_decomiso;
    private Date fecha;
    private float total;
    private String tipo_factura;
    private String estado;

    public Factura() {
    }

    public Factura(Integer idfactura) {
        this.idfactura = idfactura;
    }

    public Factura(Integer idfactura, Persona persona_idpersona, String nit, float peso_res, String decomiso, String causa_decomiso, Date fecha, float total, String tipo_factura, String estado) {
        this.idfactura = idfactura;
        this.persona_idpersona = persona_idpersona;
        this.nit = nit;
        this.peso_res = peso_res;
        this.decomiso = decomiso;
        this.causa_decomiso = causa_decomiso;
        this.fecha = fecha;
        this.total = total;
        this.tipo_factura = tipo_factura;
        this.estado = estado;
    }

    public Factura(Persona persona_idpersona, String nit, float peso_res, String decomiso, String causa_decomiso, Date fecha, float total, String tipo_factura, String estado) {
        this.persona_idpersona = persona_idpersona;
        this.nit = nit;
        this.peso_res = peso_res;
        this.decomiso = decomiso;
        this.causa_decomiso = causa_decomiso;
        this.fecha = fecha;
        this.total = total;
        this.tipo_factura = tipo_factura;
        this.estado = estado;
    }

    public Factura(Persona persona_idpersona, Date fecha, float total, String tipo_factura, String estado) {
        this.persona_idpersona = persona_idpersona;
        this.fecha = fecha;
        this.total = total;
        this.tipo_factura = tipo_factura;
        this.estado = estado;
    }

    public Integer getIdfactura() {
        return idfactura;
    }

    public void setIdfactura(Integer idfactura) {
        this.idfactura = idfactura;
    }

    public Persona getPersona_idpersona() {
        return persona_idpersona;
    }

    public void setPersona_idpersona(Persona persona_idpersona) {
        this.persona_idpersona = persona_idpersona;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public float getPeso_res() {
        return peso_res;
    }

    public void setPeso_res(float peso_res) {
        this.peso_res = peso_res;
    }

    public String getDecomiso() {
        return decomiso;
    }

    public void setDecomiso(String decomiso) {
        this.decomiso = decomiso;
    }

    public String getCausa_decomiso() {
        return causa_decomiso;
    }

    public void setCausa_decomiso(String causa_decomiso) {
        this.causa_decomiso = causa_decomiso;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public String getTipo_factura() {
        return tipo_factura;
    }

    public void setTipo_factura(String tipo_factura) {
        this.tipo_factura = tipo_factura;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
}
