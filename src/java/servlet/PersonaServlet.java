/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import controlador.ControladorPersona;
import com.google.gson.Gson;
import controlador.ControladorTipoUsuario;
import include.Ciudad;
import include.Genero;
import include.Persona;
import include.TipoUsuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.ModeloPersona;

/**
 *
 * @author Itachi
 */
public class PersonaServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /**
         * ***************************************************
         */
        /**
         * ********************* CREAR **********************
         */
        /**
         * ***************************************************
         */
        if (request.getParameter("accion").equals("crear")) {
            String cedula = request.getParameter("cedula");
            String nombres = request.getParameter("nombres");
            String apellidos = request.getParameter("apellidos");
            String direccion = request.getParameter("direccion");
            String telefono = request.getParameter("telefono");
            String contrasena = request.getParameter("contrasena");
            String email = request.getParameter("email");
            String tipoUsuarioSelect = request.getParameter("tipoUsuario");
            String celular = request.getParameter("celular");
            String fechaNacimiento = request.getParameter("fechaNacimiento");
            String generoSelect = request.getParameter("genero");
            String ciudadSelect = request.getParameter("ciudad");
            TipoUsuario tipoUsuario = new TipoUsuario(Integer.parseInt(tipoUsuarioSelect));
            Genero genero = new Genero(Integer.parseInt(generoSelect));
            Ciudad ciudad = new Ciudad(Integer.parseInt(ciudadSelect));

            /**
             * ******* FECHA NACIMIENTO ***********
             */
            SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
            Date fechaNacimientoDate = null;
            try {
                fechaNacimientoDate = new java.sql.Date(formatoDelTexto.parse(fechaNacimiento).getTime());
            } catch (ParseException ex) {
                Logger.getLogger(PersonaServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
            /**
             * *************************************
             */
            Persona persona = new Persona(cedula, nombres, apellidos, direccion, telefono, contrasena, email, tipoUsuario, celular, fechaNacimientoDate, genero, ciudad, "Activo");
            String parametro = cedula;
            ControladorPersona conPersona = new ControladorPersona();

            if (!conPersona.buscarPersona(parametro)) { //Valida que no exista la persona
                if (conPersona.crearPersona(persona)) {
                    response.getWriter().println("Persona Agregada exítosamente");
                } else {
                    response.sendError(400, "Error, No se creó la persona");
                }
            } else {
                response.sendError(400, "Error, La persona ya se encuentra registrada");
            }
            /**
             * ***********************************************************
             */
            /**
             * ************************* INACTIVAR O ACTIVAR *************
             */
            /**
             * ***********************************************************
             */
        } else if (request.getParameter("accion").equals("inactivar")) {
            String idpersona = request.getParameter("idpersona");
            String estado = request.getParameter("estado");
            String mensaje = "";
            if (estado.equals("Activo")) {
                estado = "Inactivo";
                mensaje = "Inactivada";
            } else {
                estado = "Activo";
                mensaje = "Activada";
            }
            Persona persona = new Persona();
            persona.setIdpersona(Integer.parseInt(idpersona));
            persona.setEstado(estado);
            ControladorPersona conPersona = new ControladorPersona();

            if (conPersona.buscarPersona(persona.getIdpersona().toString())) { //Valida que exista la persona
                if (conPersona.inactivarPersona(persona)) { //La inactiva o Activa
                    response.getWriter().println("Persona " + mensaje + " exítosamente");
                } else {
                    response.sendError(400, "Error, No se pudo realizar el cambio de la persona");
                }
            } else {
                response.sendError(400, "Error, La persona no se encuentra registrada");
            }
            /**
             * ************************************************************
             */
            /**
             * ************************** MODIFICAR ***********************
             */
            /**
             * ************************************************************
             */
        } else if (request.getParameter("accion").equals("modificar")) {
            Integer idpersona = Integer.parseInt(request.getParameter("idpersona"));
            String cedula = request.getParameter("cedula");
            String nombres = request.getParameter("nombres");
            String apellidos = request.getParameter("apellidos");
            String direccion = request.getParameter("direccion");
            String telefono = request.getParameter("telefono");
            String contrasena = request.getParameter("contrasena");
            String email = request.getParameter("email");
            String tipoUsuarioSelect = request.getParameter("tipoUsuario");
            String celular = request.getParameter("celular");
            String fechaNacimiento = request.getParameter("fechaNacimiento");
            String generoSelect = request.getParameter("genero");
            String ciudadSelect = request.getParameter("ciudad");

            TipoUsuario tipoUsuario = new TipoUsuario(Integer.parseInt(tipoUsuarioSelect));
            Genero genero = new Genero(Integer.parseInt(generoSelect));
            Ciudad ciudad = new Ciudad(Integer.parseInt(ciudadSelect));

            /**
             * ******* FECHA NACIMIENTO ***********
             */
            SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
            Date fechaNacimientoDate = null;
            try {
                fechaNacimientoDate = new java.sql.Date(formatoDelTexto.parse(fechaNacimiento).getTime());
            } catch (ParseException ex) {
                Logger.getLogger(PersonaServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
            /**
             * *************************************
             */
            Persona persona = new Persona(idpersona, cedula, nombres, apellidos, direccion, telefono, contrasena, email, tipoUsuario, celular, fechaNacimientoDate, genero, ciudad);
            String parametro = idpersona.toString();
            ControladorPersona conPersona = new ControladorPersona();

            if (conPersona.buscarPersona(parametro)) { //Valida que exista la persona
                if (conPersona.modificarPersona(persona)) {
                    response.getWriter().println("Persona Modificada exítosamente");
                } else {
                    response.sendError(400, "Error, No se modificó la persona");
                }
            } else {
                response.sendError(400, "Error, La persona no se encuentra registrada");
            }
            /**
             * ************************************************************
             */
            /**
             * ****************** CONSULTAR *******************************
             */
            /**
             * ************************************************************
             */
        } else if (request.getParameter("accion").equals("consultar")) {
            Persona persona = new Persona();
            String parametro = "";
            if (!request.getParameter("idpersona").equals("")) {
                persona.setIdpersona(Integer.parseInt(request.getParameter("idpersona")));
                parametro = persona.getIdpersona().toString();
            } else if (!request.getParameter("cedula").equals("")) {
                persona.setCedula(request.getParameter("cedula"));
                parametro = persona.getCedula();
            }
            ControladorPersona conPersona = new ControladorPersona();
            if (conPersona.buscarPersona(parametro)) {
                final Gson gson = new Gson();
                final String representacionJSON = gson.toJson(conPersona.consultarPersona(parametro));
                response.getOutputStream().print(representacionJSON);
                response.getOutputStream().flush();
            } else {
                response.sendError(400, "Error, La persona no se encuentra registrada");
            }
        }
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
