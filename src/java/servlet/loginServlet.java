/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import com.google.gson.Gson;
import controlador.ControladorPersona;
import include.Persona;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author camilo
 */
public class loginServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getParameter("accion").equals("entrar")) {
            Persona persona = new Persona();
            persona.setEmail(request.getParameter("email"));
            persona.setContrasena(request.getParameter("contrasena"));

            ControladorPersona conPersona = new ControladorPersona();
            Persona datosPersona = conPersona.loginPersona(persona.getEmail());//busca la persona
            if (datosPersona != null) {
                if (datosPersona.getContrasena().equals(persona.getContrasena())) {
                    HttpSession sesion = request.getSession();
                    sesion.setAttribute("usuario", datosPersona.getNombres() + " " + datosPersona.getApellidos());
                    sesion.setAttribute("idusuario", datosPersona.getIdpersona());
                    response.sendRedirect("/");
                } else {
                    response.sendError(400, "Error, Las credenciales no coinciden");
                }
            } else {
                response.sendError(400, "Error, El email no se encuentra registrado");
            }
        } else {
            response.sendError(400, "Error interno");
        }

        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
