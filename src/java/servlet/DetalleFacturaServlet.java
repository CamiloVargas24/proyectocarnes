/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import com.google.gson.Gson;
import controlador.ControladorDetalleFactura;
import controlador.ControladorFactura;
import controlador.ControladorProducto;
import include.DetalleFactura;
import include.Factura;
import include.Producto;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Itachi
 */
public class DetalleFacturaServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        if (request.getParameter("accion").equals("agregarDetalleFactura")) {

            float cantidadSolicitada = Float.parseFloat(request.getParameter("cantidad"));
            Integer idproducto = Integer.parseInt(request.getParameter("producto"));

            ControladorProducto conProducto = new ControladorProducto();
            Producto producto = new Producto();
            producto = conProducto.consultarProducto(idproducto);
            float subtotalProducto = producto.getPrecio_actual() * cantidadSolicitada;
            float nuevoPesoLibras = producto.getPeso_libras() - cantidadSolicitada;
            if (nuevoPesoLibras >= 0) {
                Integer idFactura = Integer.parseInt(request.getParameter("idfactura"));

                ControladorFactura conFactura = new ControladorFactura();
                Factura factura = conFactura.consultarFactura(idFactura);

                DetalleFactura df = new DetalleFactura(producto, factura, cantidadSolicitada, subtotalProducto);
                ControladorDetalleFactura cdf = new ControladorDetalleFactura();

                if (cdf.crearDetalleFactura(df)) {
                    producto.setPeso_libras(nuevoPesoLibras);
                    if (conProducto.modificarProducto(producto)) { //actualiza la nueva cantidad de libras de carne que queda disponible para vender
                        factura.setTotal(factura.getTotal() + subtotalProducto);
                        if (conFactura.modificarFactura(factura)) {
                            response.getWriter().println(cdf.consultarDetalleFactura(idFactura));
                        } else {
                            response.sendError(400, "Error, Se produjo al modificar la factura");
                        }
                    } else {
                        response.sendError(400, "Error, Se produjo un error al actualizar el producto");
                    }

                } else {
                    response.sendError(400, "Error, No se agrego el producto a la factura");
                }
            } else {
                response.sendError(400, "Error, No hay suficiente carne, quedan " + producto.getPeso_libras() + " libras");
            }
        } else if (request.getParameter("accion").equals("eliminarProductoDetalleFactura")) {
            ControladorProducto conp = new ControladorProducto();
            Integer idproducto = Integer.parseInt(request.getParameter("idproducto"));
            if (conp.buscarProducto(idproducto)) { //Consulta el producto
                ControladorFactura conf = new ControladorFactura();
                Integer idfactura = Integer.parseInt(request.getParameter("idfactura"));
                if (conf.buscarFactura(idfactura)) { //Consulta la factura
                    ControladorDetalleFactura condf = new ControladorDetalleFactura();
                    DetalleFactura detalleFactura = new DetalleFactura();
                    detalleFactura = condf.consultarDetalleFactura(idfactura, idproducto);
                    float cantidad_a_descontar = detalleFactura.getPeso_producto();
                    float dinero_a_descontar = detalleFactura.getSubtotal();
                    Producto producto = new Producto();
                    producto = conp.consultarProducto(idproducto);
                    producto.setPeso_libras(producto.getPeso_libras() + cantidad_a_descontar); //Aumenta el stock del producto
                    Factura factura = new Factura();
                    factura = conf.consultarFactura(idfactura);
                    factura.setTotal(factura.getTotal() - dinero_a_descontar);
                    if (conf.modificarFactura(factura)) {
                        if (conp.modificarProducto(producto)) {
                            if (condf.eliminarDetalleFactura(idfactura, idproducto)) {
                                response.getWriter().println(condf.consultarDetalleFactura(idfactura));
                            } else {
                                response.sendError(400, "Error, Error eliminar el producto de la factura");
                            }
                        } else {
                            response.sendError(400, "Error, Error al modificar el producto");
                        }
                    } else {
                        response.sendError(400, "Error, Error al modificar la factura");
                    }
                } else {
                    response.sendError(400, "Error, No se encontró la factura");
                }
            } else {
                response.sendError(400, "Error, No se encontró el producto");
            }
        } else if (request.getParameter("accion").equals("consultarDetalleFactura")) {
            ControladorFactura conf = new ControladorFactura();
            Integer idfactura = Integer.parseInt(request.getParameter("idfactura"));
            if (conf.buscarFactura(idfactura)) { //Consulta la factura
                ControladorDetalleFactura condf = new ControladorDetalleFactura();
                response.getWriter().println(condf.consultarDetalleFactura(idfactura));
            } else {
                response.sendError(400, "Error, No se encontró la factura");
            }
        } else if (request.getParameter("accion").equals("agregarDetalleFacturaPedido")) {

            float cantidadSolicitada = Float.parseFloat(request.getParameter("cantidad"));
            float precioNuevo = Float.parseFloat(request.getParameter("precioNuevo"));
            Integer idproducto = Integer.parseInt(request.getParameter("producto"));

            ControladorProducto conProducto = new ControladorProducto();
            Producto producto = new Producto();
            producto = conProducto.consultarProducto(idproducto);
            producto.setPrecio_actual(precioNuevo);
            float subtotalProducto = producto.getPrecio_actual() * cantidadSolicitada;
            float nuevoPesoLibras = producto.getPeso_libras() + cantidadSolicitada;

            Integer idFactura = Integer.parseInt(request.getParameter("idfactura"));

            ControladorFactura conFactura = new ControladorFactura();
            Factura factura = conFactura.consultarFactura(idFactura);

            DetalleFactura df = new DetalleFactura(producto, factura, cantidadSolicitada, subtotalProducto);
            ControladorDetalleFactura cdf = new ControladorDetalleFactura();

            if (cdf.crearDetalleFactura(df)) {
                producto.setPeso_libras(nuevoPesoLibras);
                if (conProducto.modificarProducto(producto)) { //actualiza la nueva cantidad de libras de carne que queda disponible para vender
                    factura.setTotal(factura.getTotal() + subtotalProducto);
                    if (conFactura.modificarFactura(factura)) {
                        response.getWriter().println(cdf.consultarDetalleFactura(idFactura));
                    } else {
                        response.sendError(400, "Error, Se produjo al modificar la factura");
                    }
                } else {
                    response.sendError(400, "Error, Se produjo un error al actualizar el producto");
                }

            } else {
                response.sendError(400, "Error, No se agrego el producto a la factura");
            }

        }else if (request.getParameter("accion").equals("eliminarProductoDetalleFacturaPedido")) {
            ControladorProducto conp = new ControladorProducto();
            Integer idproducto = Integer.parseInt(request.getParameter("idproducto"));
            if (conp.buscarProducto(idproducto)) { //Consulta el producto
                ControladorFactura conf = new ControladorFactura();
                Integer idfactura = Integer.parseInt(request.getParameter("idfactura"));
                if (conf.buscarFactura(idfactura)) { //Consulta la factura
                    ControladorDetalleFactura condf = new ControladorDetalleFactura();
                    DetalleFactura detalleFactura = new DetalleFactura();
                    detalleFactura = condf.consultarDetalleFactura(idfactura, idproducto);
                    float cantidad_a_descontar = detalleFactura.getPeso_producto();
                    float dinero_a_descontar = detalleFactura.getSubtotal();
                    Producto producto = new Producto();
                    producto = conp.consultarProducto(idproducto);
                    producto.setPeso_libras(producto.getPeso_libras() - cantidad_a_descontar); //Disminuye el stock del producto
                    Factura factura = new Factura();
                    factura = conf.consultarFactura(idfactura);
                    factura.setTotal(factura.getTotal() - dinero_a_descontar);
                    if (conf.modificarFactura(factura)) {
                        if (conp.modificarProducto(producto)) {
                            if (condf.eliminarDetalleFactura(idfactura, idproducto)) {
                                response.getWriter().println(condf.consultarDetalleFactura(idfactura));
                            } else {
                                response.sendError(400, "Error, Error eliminar el producto de la factura");
                            }
                        } else {
                            response.sendError(400, "Error, Error al modificar el producto");
                        }
                    } else {
                        response.sendError(400, "Error, Error al modificar la factura");
                    }
                } else {
                    response.sendError(400, "Error, No se encontró la factura");
                }
            } else {
                response.sendError(400, "Error, No se encontró el producto");
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
