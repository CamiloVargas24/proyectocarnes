/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import com.google.gson.Gson;
import controlador.ControladorFactura;
import controlador.ControladorPersona;
import controlador.ControladorProducto;
import include.Factura;
import include.Persona;
import include.Producto;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Camilo Vargas
 */
public class PedidoServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /**
         * ***************** CONSULTAR *********************************
         */
        if (request.getParameter("accion").equals("consultarProducto")) {
            Producto producto = new Producto();
            producto.setIdproducto(Integer.parseInt(request.getParameter("idproducto")));

            ControladorProducto conProducto = new ControladorProducto();
            final Gson gson = new Gson();
            final String representacionJSON = gson.toJson(conProducto.consultarProducto(producto.getIdproducto()));
            response.getOutputStream().print(representacionJSON);
            response.getOutputStream().flush();
            /**
             * *********************** GUARDAR FACTURA ***********************
             */
        } else if (request.getParameter("accion").equals("guardarFactura")) {
            Persona persona = new Persona(Integer.parseInt(request.getParameter("idpersona")));
            final Gson gson = new Gson();
            ControladorPersona conPersona = new ControladorPersona();
            if (conPersona.buscarPersona(persona.getIdpersona().toString())) { //Valida si el proveedor existe
                Factura factura = new Factura();
                factura.setPersona_idpersona(persona);
                factura.setPeso_res(Float.parseFloat(request.getParameter("peso_total")));
                factura.setDecomiso(request.getParameter("decomisado"));
                factura.setCausa_decomiso(request.getParameter("causa_decomiso"));
                factura.setTipo_factura("Pedido");
                factura.setEstado("Activo");

                ControladorFactura conFactura = new ControladorFactura();
                if (conFactura.crearFacturaPedido(factura)) {
                    String representacionJSON = gson.toJson(conFactura.consultarUltimoIDFactura());
                    response.getOutputStream().print(representacionJSON);
                    response.getOutputStream().flush();
                } else {
                    response.sendError(400, "Error, No se creo la factura");
                }
            } else {
                response.sendError(400, "Error, La persona no se encuentra registrada");
            }
        } else if (request.getParameter("accion").equals("consultarPedido")) {
            ControladorFactura conf = new ControladorFactura();
            Integer idfactura = Integer.parseInt(request.getParameter("idfactura"));
            if (conf.buscarFactura(idfactura)) { //Consulta la factura
                Factura fac = conf.consultarFactura(idfactura);
                String tipoFactura = fac.getTipo_factura();
                if (tipoFactura.equals("Pedido")) {
                    final Gson gson = new Gson();
                    final String representacionJSON = gson.toJson(fac);
                    response.getOutputStream().print(representacionJSON);
                    response.getOutputStream().flush();
                } else {
                    response.sendError(400, "Error, El id a buscar es de una venta");
                }
            } else {
                response.sendError(400, "Error, No se encontro el pedido");
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
