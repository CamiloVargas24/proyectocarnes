/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import com.google.gson.Gson;
import controlador.ControladorProducto;
import include.Producto;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author camilo
 */
public class ProductoServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        /**
         * ***************************************************
         */
        /**
         * ********************* CREAR **********************
         */
        /**
         * ***************************************************
         */
        if (request.getParameter("accion").equals("crear")) {
            Integer idproducto = null;
            String nombre = request.getParameter("nombre");
            if(!request.getParameter("idproducto").equals("")){
                idproducto = Integer.parseInt(request.getParameter("idproducto"));
            }
            
            String descripcion = request.getParameter("descripcion");
            Float precio = Float.parseFloat(request.getParameter("precio"));
            Float peso = Float.parseFloat(request.getParameter("peso"));

            /**
             * *************************************
             */
            Producto producto = new Producto(idproducto, nombre, descripcion, precio, peso, null, "Activo");
            ControladorProducto conProducto = new ControladorProducto();
            if (idproducto != null) {
                if (!conProducto.buscarProducto(idproducto)) { //Valida que no exista la persona
                    if (conProducto.crearProducto(producto)) {
                        response.getWriter().println("Producto Agregado exítosamente");
                    } else {
                        response.sendError(400, "Error, No se creó el producto");
                    }
                } else {
                    response.sendError(400, "Error, El producto ya se encuentra registrado");
                }
            } else {
                if (conProducto.crearProducto(producto)) {
                    response.getWriter().println("Producto Agregado exítosamente");
                } else {
                    response.sendError(400, "Error, No se creó el producto");
                }
            }

            /**
             * ***********************************************************
             */
            /**
             * ************************* INACTIVAR O ACTIVAR *************
             */
            /**
             * ***********************************************************
             */
        } else if (request.getParameter("accion").equals("inactivar")) {
            String idproducto = request.getParameter("id");
            String estado = request.getParameter("estado");
            String mensaje = "";
            if (estado.equals("Activo")) {
                estado = "Inactivo";
                mensaje = "Inactivado";
            } else {
                estado = "Activo";
                mensaje = "Activado";
            }
            Producto producto = new Producto();
            producto.setIdproducto(Integer.parseInt(idproducto));
            producto.setEstado(estado);
            ControladorProducto conProducto = new ControladorProducto();
            if (conProducto.buscarProducto(producto.getIdproducto())) { //Valida que exista el producto
                if (conProducto.inactivarProducto(producto)) { //La inactiva o Activa
                    response.getWriter().println("Producto " + mensaje + " exítosamente");
                } else {
                    response.sendError(400, "Error, No se pudo realizar el cambio de estado");
                }
            } else {
                response.sendError(400, "Error, El producto no se encuentra registrado");
            }
            /**
             * ************************************************************
             */
            /**
             * ************************** MODIFICAR ***********************
             */
            /**
             * ************************************************************
             */
        } else if (request.getParameter("accion").equals("modificar")) {
            Integer idproducto = Integer.parseInt(request.getParameter("idproducto"));
            String nombre = request.getParameter("nombre");
            String descripcion = request.getParameter("descripcion");
            Float precio = Float.parseFloat(request.getParameter("precio"));
            Float peso = Float.parseFloat(request.getParameter("peso"));

            /**
             * *************************************
             */
            Producto producto = new Producto(idproducto, nombre, descripcion, precio, peso, null, "Activo");
            ControladorProducto conProducto = new ControladorProducto();
            if (conProducto.buscarProducto(idproducto)) { //Valida que exista el producto
                if (conProducto.modificarProducto(producto)) {
                    response.getWriter().println("Producto Modificado exítosamente");
                } else {
                    response.sendError(400, "Error, No se modificó el producto");
                }
            } else {
                response.sendError(400, "Error, El producto no se encuentra registrado");
            }
            /**
             * ************************************************************
             */
            /**
             * ****************** CONSULTAR *******************************
             */
            /**
             * ************************************************************
             */
        } else if (request.getParameter("accion").equals("consultar")) {
            Producto producto = new Producto();
            producto.setIdproducto(Integer.parseInt(request.getParameter("idproducto")));

            ControladorProducto conProducto = new ControladorProducto();
            if (conProducto.buscarProducto(producto.getIdproducto())) {
                final Gson gson = new Gson();
                final String representacionJSON = gson.toJson(conProducto.consultarProducto(producto.getIdproducto()));
                response.getOutputStream().print(representacionJSON);
                response.getOutputStream().flush();
            } else {
                response.sendError(400, "Error, El producto no se encuentra registrado");
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
