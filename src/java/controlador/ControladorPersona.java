/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import include.Persona;
import modelo.ModeloPersona;

/**
 *
 * @author Itachi
 */
public class ControladorPersona {

    public boolean crearPersona(Persona persona) {
        ModeloPersona modelop = new ModeloPersona();
        return modelop.crearPersona(persona);
    }

    public boolean buscarPersona(String parametro) {
        ModeloPersona modelop = new ModeloPersona();
        return modelop.buscarPersona(parametro);
    }

    public Persona consultarPersona(String parametro) {
        ModeloPersona modelop = new ModeloPersona();
        return modelop.consultarPersona(parametro);
    }

    public Boolean modificarPersona(Persona persona) {
        ModeloPersona modelop = new ModeloPersona();
        return modelop.modificarPersona(persona);
    }

    public Boolean inactivarPersona(Persona persona) {
        ModeloPersona modelop = new ModeloPersona();
        return modelop.inactivarPersona(persona);
    }

    public String getViewPersonas() {
        String htmlcode = "";
        String sqlProveedores = " AND tipo_usuario_idtipo_usuario = 2";
        ModeloPersona modelop = new ModeloPersona();
        for (Persona m : modelop.getAllPersona(sqlProveedores)) {
            htmlcode += "<option value=" + m.getIdpersona() + ">" + m.getCedula() + " - " + m.getNombres() + " " + m.getApellidos() + "</option>";
        }
        return htmlcode;
    }
    
    public String getViewProveedores() {
        String htmlcode = "";
        String sqlProveedores = " AND tipo_usuario_idtipo_usuario = 3";
        ModeloPersona modelop = new ModeloPersona();
        for (Persona m : modelop.getAllPersona(sqlProveedores)) {
            htmlcode += "<option value=" + m.getIdpersona() + ">" + m.getCedula() + " - " + m.getNombres() + " " + m.getApellidos() + "</option>";
        }
        return htmlcode;
    }
    public Persona loginPersona(String email) {
        ModeloPersona modelop = new ModeloPersona();
        return modelop.loginPersona(email);
    }
}
