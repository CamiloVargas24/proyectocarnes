/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador;

import include.Ciudad;
import modelo.ModeloCiudad;

/**
 *
 * @author wilmar
 */
public class ControladorCiudad {
    public  String getViewCiudades(){
        String htmlcode = "";
        ModeloCiudad modeloc = new ModeloCiudad();
        for (Ciudad c : modeloc.getAllCiudad()) {
            htmlcode += "<option value="+c.getIdciudad()+">"+c.getCiudad()+"</option>";
        }
        return htmlcode;
    }
}
