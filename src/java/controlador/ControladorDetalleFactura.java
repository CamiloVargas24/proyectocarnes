/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import include.DetalleFactura;
import include.Factura;
import include.Producto;
import java.util.ArrayList;
import modelo.ModeloDetalleFactura;
import modelo.ModeloFactura;
import modelo.ModeloProducto;

/**
 *
 * @author wilmar
 */
public class ControladorDetalleFactura {

    public boolean crearDetalleFactura(DetalleFactura detalleFactura) {
        ModeloDetalleFactura modelodf = new ModeloDetalleFactura();
        return modelodf.crearDetalleFactura(detalleFactura);
    }

    public DetalleFactura consultarDetalleFactura(Integer idfactura, Integer idproducto) {
        ModeloDetalleFactura modelodf = new ModeloDetalleFactura();
        return modelodf.consultarDetalleFactura(idfactura, idproducto);
    }

    public String consultarDetalleFactura(Integer idFactura) {
        String htmlcode = "";
        ModeloDetalleFactura modelodf = new ModeloDetalleFactura();
        for (DetalleFactura df : modelodf.consultarDetalleFactura(idFactura)) {
            ModeloProducto mp = new ModeloProducto();
            Producto p = new Producto();
            p = mp.consultarProducto(df.getProducto_idproducto().getIdproducto());

//            ModeloFactura mf = new ModeloFactura();
//            Factura f = new Factura();
//            f = mf.crearFacturaVenta(f)
            htmlcode += "<tr>"
                    + "<td style=\"background: #bbffdd;\">" + p.getNombre() + "</td>"
                    + "<td style=\"background: #bbffdd;\">" + df.getPeso_producto() + "</td>"
                    + "<td style=\"background: #bbffdd;\">" + p.getPrecio_actual() + "</td>"
                    + "<td style=\"background: #bbffdd;\">" + df.getSubtotal() + "</td>"
                    + "<td><input type=\"submit\" value=\"-\" onclick=\"eliminarProductoFactura(event)\" idfactura=\"" + idFactura + "\" idproducto=\"" + p.getIdproducto() + "\"></td>"
                    + "</tr>";
        }
        ControladorFactura conFac = new ControladorFactura();
        Factura factura = conFac.consultarFactura(idFactura);
        htmlcode += "<tr>"
                + "<td colspan=\"2\"></td>\n"
                + "<td>Total</td>\n"
                + "<td colspan=\"2\">" + factura.getTotal() + "</td>"
                + "</tr>";
        return htmlcode;
    }

    public boolean eliminarDetalleFactura(Integer idfactura, Integer idproducto) {
        ModeloDetalleFactura modelodf = new ModeloDetalleFactura();
        return modelodf.eliminarDetalleFactura(idfactura, idproducto);
    }
}
