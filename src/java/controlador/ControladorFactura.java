/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import include.Factura;
import modelo.ModeloFactura;

/**
 *
 * @author Itachi
 */
public class ControladorFactura {

    public Boolean crearFacturaVenta(Factura factura) {
        ModeloFactura modelof = new ModeloFactura();
        return modelof.crearFacturaVenta(factura);
    }
    
    public Boolean crearFacturaPedido(Factura factura) {
        ModeloFactura modelof = new ModeloFactura();
        return modelof.crearFacturaPedido(factura);
    }

    public boolean buscarFactura(Integer idfactura) {
        ModeloFactura modelof = new ModeloFactura();
        return modelof.buscarFactura(idfactura);
    }

    public Factura consultarFactura(Integer idFactura) {
        ModeloFactura modelof = new ModeloFactura();
        return modelof.consultarFactura(idFactura);
    }

    public Integer consultarUltimoIDFactura() {
        ModeloFactura modelof = new ModeloFactura();
        return modelof.consultarUltimoIDFactura();
    }

    public Boolean modificarFactura(Factura factura) {
        ModeloFactura modelof = new ModeloFactura();
        return modelof.modificarFactura(factura);
    }

}
