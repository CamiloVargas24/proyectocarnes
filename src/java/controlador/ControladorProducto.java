/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador;

import include.Producto;
import modelo.ModeloProducto;

/**
 *
 * @author Itachi
 */
public class ControladorProducto {
    
    public boolean crearProducto(Producto producto) {
        ModeloProducto modelop = new ModeloProducto();
        return modelop.crearProducto(producto);
    }
    public boolean buscarProducto(Integer idproducto) {
        ModeloProducto modelop = new ModeloProducto();
        return modelop.buscarProducto(idproducto);
    }
    
    public Producto consultarProducto(Integer idproducto) {
        ModeloProducto modelop = new ModeloProducto();
        return modelop.consultarProducto(idproducto);
    }
    
    public Boolean modificarProducto(Producto producto) {
        ModeloProducto modelop = new ModeloProducto();
        return modelop.modificarProducto(producto);
    }
    
    public Boolean inactivarProducto(Producto producto) {
        ModeloProducto modelop = new ModeloProducto();
        return modelop.inactivarProducto(producto);
    }
    
    public  String getViewProductos(){
        String htmlcode = "<option>Seleccione</option>";
        ModeloProducto modelop = new ModeloProducto();
        for (Producto p : modelop.getAllProducto()) {
            htmlcode += "<option value="+p.getIdproducto()+">"+p.getNombre()+"</option>";
        }
        return htmlcode;
    }
     
}
