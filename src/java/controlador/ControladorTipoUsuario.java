/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador;

import include.TipoUsuario;
import modelo.ModeloTipoUsuario;

/**
 *
 * @author Itachi
 */
public class ControladorTipoUsuario {
    public  String getViewTipoUsuarios(){
        String htmlcode = "";
        ModeloTipoUsuario modelop = new ModeloTipoUsuario();
        for (TipoUsuario m : modelop.getAllTipoUsuario()) {
            htmlcode += "<option value="+m.getIdtipo_usuario()+">"+m.getTipo()+"</option>";
        }
        return htmlcode;
    }
}
